from gi.repository import Gtk, Gio, GObject
import gi

gi.require_version("Gtk", "4.0")


class DataObject(GObject.GObject):

    __gtype_name__ = 'DataObject'

    buffer = GObject.Property(type=object, default=None)

    def __init__(self, text):

        super().__init__()

        self.buffer = Gtk.TextBuffer.new()
        self.buffer.set_text(text)


def setup(widget, item):
    """Setup the widget to show in the Gtk.Listview"""
    text_view = Gtk.TextView.new()

    text_view.set_wrap_mode(Gtk.WrapMode.WORD)    
    item.set_child(text_view)


def bind(widget, item):
    """bind data from the store object to the widget"""
    text_view = item.get_child()
    obj = item.get_item()

    text_view.set_buffer(obj.buffer)


def on_activate(app):
    win = Gtk.ApplicationWindow(
        application=app,
        title="Gtk4 is Awesome !!!",
        default_height=400,
        default_width=400,
    )
    sw = Gtk.ScrolledWindow()
    list_view = Gtk.ListView()
    factory = Gtk.SignalListItemFactory()
    factory.connect("setup", setup)
    factory.connect("bind", bind)

    list_view.set_factory(factory)

    selection = Gtk.SingleSelection()

    store = Gio.ListStore.new(DataObject)

    selection.set_model(store)

    list_view.set_model(selection)

    text = "Type definition for a function that will be called back when an asynchronous operation within GIO has been completed. GAsyncReadyCallback callbacks from GTask are guaranteed to be invoked in a later iteration of the [thread-default main context][g-main-context-push-thread-default] where the GTask was created. All other users of GAsyncReadyCallback must likewise call it asynchronously in a later iteration of the main context."

    v1 = DataObject(text)
    v2 = DataObject("other")

    store.append(v1)
    store.append(v2)

    sw.set_child(list_view)
    win.set_child(sw)
    win.present()


app = Gtk.Application(application_id="org.gtk.Example")
app.connect("activate", on_activate)
app.run(None)

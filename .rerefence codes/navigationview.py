from gi.repository import Gtk


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/NavigationView.ui')
class NavigationView(Gtk.Box):

    __gtype_name__ = 'NavigationView'

    _navigation_view = Gtk.Template.Child()
    _tree_view = Gtk.Template.Child()
    _main_stack = Gtk.Template.Child()
    _main_text_view = Gtk.Template.Child()
    _status_page = Gtk.Template.Child()    
    _overlay = Gtk.Template.Child()
    _revealer_searchbar = Gtk.Template.Child()
    _notes_board = Gtk.Template.Child()
    _left_stack = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._on_initial()

    def _on_initial(self):

        self._main_stack.set_visible_child(self._overlay)
        


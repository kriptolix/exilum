from gi.repository import Gtk, Adw, GObject, Gio

import re


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/SearchBar.ui')
class SearchBar(Adw.Bin):

    __gtype_name__ = 'SearchBar'

    replace_mode_enabled = GObject.property(type=bool, default=False)
    search_mode_enabled = GObject.property(type=bool, default=False)

    _search_bar = Gtk.Template.Child()
    _search_entry = Gtk.Template.Child()
    _previous_result_button = Gtk.Template.Child()
    _next_result_button = Gtk.Template.Child()
    _regex_toggle = Gtk.Template.Child()
    _case_toggle = Gtk.Template.Child()
    _replace_toggle = Gtk.Template.Child()
    _replace_entry = Gtk.Template.Child()    
    _replace_revealer = Gtk.Template.Child()    
    _replace_one = Gtk.Template.Child()
    _replace_all = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        # self._view = view
        # self._buffer = buffer

        self.matches = []
        self.active = 0

        self.connect("notify::search-mode-enabled", self.search_enabled)
        self.connect("notify::replace-mode-enabled", self.replace_enabled)

        # self._replace_toggle.bind("active", self, "replace_mode_enabled",
        #                           Gio.SettingsBindFlags.DEFAULT)


    def search_enabled(self, *args, **kwargs):
        if self.searchbar.get_search_mode():
            self._buffer = self._view.get_buffer()
            if self._buffer.get_has_selection():
                self.search_entry.set_text(self._buffer.get_slice(
                    *self._buffer.get_selection_bounds(), False))
            self.search_entry.grab_focus()
            self.search_entry.select_region(0, -1)
            self.search()
        else:
            self._buffer.remove_tag(self.highlight,
                                    self._buffer.get_start_iter(),
                                    self._buffer.get_end_iter())
            self.matches = []
            self.replace_mode_enabled = False
            self._view.grab_focus()

    def replace_enabled(self, _widget, _data):
        if self.replace_mode_enabled and not self.search_mode_enabled:
            self.search_mode_enabled = True

    def search(self, _widget=None, _data=None, scroll=True):
        if not self._buffer:
            return
        searchtext = self.search_entry.get_text()
        context_start = self._buffer.get_start_iter()
        context_end = self._buffer.get_end_iter()
        text = self._buffer.get_slice(context_start, context_end, False)

        self._buffer.remove_tag(self.highlight, context_start, context_end)

        # case sensitive?
        flags = False
        if not self.case_sensitive.get_active():
            flags = flags | re.I

        # regex?
        if not self.regex.get_active():
            searchtext = re.escape(searchtext)

        matches = re.finditer(searchtext, text, flags)

        self.matches = []
        self.active = 0
        for match in matches:
            self.matches.append((match.start(), match.end()))
            start_iter = self._buffer.get_iter_at_offset(match.start())
            end_iter = self._buffer.get_iter_at_offset(match.end())
            self._buffer.apply_tag(self.highlight, start_iter, end_iter)
        if scroll:
            self.scrollto(self.active)
        # LOGGER.debug(searchtext)

    def scrollto(self, index):
        if not self.matches:
            return
        self.active = index % len(self.matches)

        match = self.matches[self.active]

        start_iter = self._buffer.get_iter_at_offset(match[0])
        end_iter = self._buffer.get_iter_at_offset(match[1])

        # create a mark at the start of the coincidence to scroll to it
        mark = self._buffer.create_mark(None, start_iter, False)
        self._view.scroller.smooth_scroll_to_mark(mark, center=True)

        # select coincidence
        self._buffer.select_range(start_iter, end_iter)

    def hide(self, *arg, **kwargs):
        self.search_mode_enabled == False

    def replace_clicked(self, _widget, _data=None):
        self.replace(self.active)

    def replace_all(self, _widget=None, _data=None):
        with user_action(self._buffer):
            for match in reversed(self.matches):
                self.__do_replace(match)
        self.search(scroll=False)

    def replace(self, searchindex, _inloop=False):
        with user_action(self._buffer):
            self.__do_replace(self.matches[searchindex])
        active = self.active
        self.search(scroll=False)
        self.active = active
        self.scrollto(self.active)

    def __do_replace(self, match):
        start_iter = self._buffer.get_iter_at_offset(match[0])
        end_iter = self._buffer.get_iter_at_offset(match[1])
        self._buffer.delete(start_iter, end_iter)
        start_iter = self._buffer.get_iter_at_offset(match[0])
        self._buffer.insert(start_iter, self.replace_entry.get_text())

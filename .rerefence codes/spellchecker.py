import gi
gi.require_version('Gspell', '1')
from gi.repository import Gspell


class Spellchecker(object):

    def __init__(self, source_view):
        self.checker = Gspell.Checker()
        self.spell_buffer = Gspell.TextBuffer.get_from_gtk_text_buffer(source_view.get_buffer())
        self.spell_buffer.set_spell_checker(self.checker)
        self.spell_view = Gspell.TextView.get_from_gtk_text_view(source_view)
        self.spell_view.set_enable_language_menu(False)
        self.spell_navigator = Gspell.NavigatorTextView.new(source_view)

    def set_enabled(self, value):
        self.spell_view.set_inline_spell_checking(value)

    def set_language(self, language_code):
        if not language_code:
            language = Gspell.Language.lookup(language_code)
        else:
            language = None
        self.checker.set_language(language)
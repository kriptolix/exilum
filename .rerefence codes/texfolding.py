
class CodeFolding():

    def __init__(self, buffer):

        self.buffer = buffer

        self.blocks = list()
        self.marks_start = dict()
        self.folding_regions = dict()
        self.folding_regions_by_id = dict()
        self.maximum_region_id = 0
        self.initial_folded_regions_set = False
        self.initial_folding_done = False
        self.initial_folding_regions_checked_count = 0

        self.buffer('text_inserted', self.on_text_inserted)
        self.buffer('text_deleted', self.on_text_deleted)
        self.buffer('buffer_changed', self.on_buffer_changed)

    def on_text_inserted(self, content, parameter):
        buffer, location_iter, text, text_length = parameter
        length = len(text)
        offset = location_iter.get_offset() + length
        marks_start = dict()
        
        for index, region_id in self.marks_start.items():
            if index < offset:
                marks_start[index] = region_id
            else:
                index2 = index + length
                region = self.folding_regions_by_id[region_id]
                region['offset_start'] = index2
                marks_start[index2] = region_id
        self.marks_start = marks_start

    def on_text_deleted(self, content, parameter):
        buffer, start_iter, end_iter = parameter
        offset_start = start_iter.get_offset()
        offset_end = end_iter.get_offset()
        length = offset_end - offset_start
        marks_start = dict()
        for index, region_id in self.marks_start.items():
            if index <= offset_start:
                marks_start[index] = region_id
            elif index >= offset_end:
                index2 = index - length
                region = self.folding_regions_by_id[region_id]
                region['offset_start'] = index2
                marks_start[index2] = region_id
        self.marks_start = marks_start

    def on_buffer_changed(self, content, parameter):
        if self.is_enabled:
            self.update_folding_regions()    

    def get_folding_region_by_region_id(self, region_id):
        return self.folding_regions_by_id[region_id]

    # @timer
    def update_folding_regions(self):
        folding_regions = dict()
        folding_regions_by_region_id = dict()
        last_line = -1
        try:
            blocks = self.document.get_blocks()
        except AttributeError:
            return
        if not self.blocks_changed(blocks):
            return
        self.blocks = blocks

        for block in blocks:
            if (block[1]):
                if block[2] != last_line:
                    region_id = self.get_mark_start_at_offset(block[0])
                    if (region_id):
                        region_dict = self.get_folding_region_by_region_id(
                            region_id)
                        region_dict['starting_line'] = block[2]
                        region_dict['ending_line'] = block[3]
                        region_dict['offset_end'] = block[1]
                        folding_regions_by_region_id[region_id] = region_dict
                    else:
                        self.add_mark_start(self.maximum_region_id, block[0])
                        region_dict = {'offset_start': block[0],
                                       'offset_end': block[1],
                                       'is_folded': False,
                                       'starting_line': block[2],
                                       'ending_line': block[3],
                                       'id': self.maximum_region_id}

                        folding_regions_by_region_id[self.maximum_region_id] = region_dict
                        self.maximum_region_id += 1
                    folding_regions[block[2]] = region_dict
                last_line = block[2]

        self.delete_invalid_regions(folding_regions_by_region_id)

        self.folding_regions = folding_regions
        self.folding_regions_by_id = folding_regions_by_region_id

        if not self.initial_folding_done:
            self.initial_folding()

    # @timer
    def delete_invalid_regions(self, folding_regions_by_region_id):
        regions_to_delete = [
            region_id for region_id in self.folding_regions_by_id if region_id not in folding_regions_by_region_id]
        for region_id in regions_to_delete:
            region = self.folding_regions_by_id[region_id]
            self.toggle_folding_region(
                region, show_region_regardless_of_state=True)
            self.delete_mark_start(region['offset_start'])

    def add_mark_start(self, region_id, offset):
        self.marks_start[offset] = region_id

    def delete_mark_start(self, offset):
        try:
            del (self.marks_start[offset])
        except KeyError:
            pass

    def get_mark_start_at_offset(self, offset):
        if offset in self.marks_start:
            return self.marks_start[offset]
        return None

    # @timer
    def blocks_changed(self, blocks):
        blocks_old = self.blocks
        if len(blocks) != len(blocks_old):
            return True
        for block_old, block_new in zip(blocks_old, blocks):
            if block_old[0] != block_new[0] or block_old[1] != block_new[1]:
                return True
        return False

    def get_folded_regions(self):
        folded_regions = list()
        for region in self.folding_regions.values():
            if region['is_folded']:
                folded_regions.append(
                    {'starting_line': region['starting_line'],
                     'ending_line': region['ending_line']})
        return folded_regions

    


class CodeFoldingPresenter(object):

    def __init__(self, model):
        self.model = model
        self.source_buffer = self.model.document.content.source_buffer
        self.tag = "invisible"

    def show_region(self, region):
        offset_start = region['offset_start']
        start_iter = self.source_buffer.get_iter_at_offset(offset_start)
        start_iter.forward_to_line_end()
        offset_end = region['offset_end']
        end_iter = self.source_buffer.get_iter_at_offset(offset_end)
        if not end_iter.ends_line():
            end_iter.forward_to_line_end()
        end_iter.forward_char()
        self.source_buffer.remove_tag(self.tag, start_iter, end_iter)
        for some_region in self.model.folding_regions.values():
            if some_region['is_folded']:
                if (some_region['starting_line'] >= region['starting_line']
                        and some_region['ending_line'] <= region['ending_line']):
                    self.hide_region(some_region)

    def hide_region(self, region):
        offset_start = region['offset_start']
        start_iter = self.source_buffer.get_iter_at_offset(offset_start)
        start_iter.forward_to_line_end()
        offset_end = region['offset_end']
        end_iter = self.source_buffer.get_iter_at_offset(offset_end)
        if not end_iter.ends_line():
            end_iter.forward_to_line_end()
        end_iter.forward_char()
        self.source_buffer.apply_tag(self.tag, start_iter, end_iter)

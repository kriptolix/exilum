from gi.repository import Gtk, Pango


class TagManager(): # refazer essa merda toda, tá tudo errado

    def __init__(self, tag_table):

        # Gtk.TextTag.new()
        # self._tag_table.add()

        format_tags = [
            ["italic", "style", Pango.Style.ITALIC],
            ["bold", "weight", 800]
        ]

        for tag in format_tags:
            new = Gtk.TextTag.new(tag[0]) 
            setattr(new, tag[1], tag[2])

        
        # self._buffer.create_tag('search_highlight', background="yellow")

        # color = Gdk.RGBA()
        # color.parse('#7F7F7F')
        # color.to_string()  # 'rgb(127,127,127)'

    def _create_tag(self, tag_name, tag_table):
        
        tag = Gtk.TextTag.new(tag_name)
        tag_table.add(tag)
    
    def _create_element_tag(self, name: str, buffer) -> None:

        buffer.create_tag(name, foreground="blue")

    def _apply_tag(self, name: str, buffer) -> None:

        selection = buffer.get_selection_bounds()

        # mark 0
        if (selection[0].ends_word()):

            selection[0].forward_visible_word_end()
            selection[0].backward_visible_word_start()

        elif (selection[0].inside_word()):
            selection[0].backward_visible_word_start()

        # mark 1
        if (selection[1].starts_word()):

            selection[1].backward_visible_word_start()
            selection[1].forward_visible_word_end()

        elif (selection[1].inside_word()):
            selection[1].forward_visible_word_end()

        buffer.apply_tag_by_name(name, selection[0], selection[1])

    def _remove_tag_from_buffer(self, name: str) -> None:

        tag = self._tag_table.lookup(name)
        self._tag_table.remove(tag)

    def _remove_tag_from_range(self,
                               buffer,
                               name: str,
                               start: int,
                               end: int) -> None:

        buffer.remove_tag_by_name(name, start, end)

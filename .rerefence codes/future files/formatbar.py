from gi.repository import Gtk

import datetime

from ...tagmanager import TagManager


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/FormatBar.ui')
class FormatBar(Gtk.Box):

    __gtype_name__ = 'FormatBar'

    _format_stack = Gtk.Template.Child()
    _main_view = Gtk.Template.Child()
    _header_button = Gtk.Template.Child()
    _justify_button = Gtk.Template.Child()
    _highligh_button = Gtk.Template.Child()
    _create_note_button = Gtk.Template.Child()
    _create_keyword_button = Gtk.Template.Child()
    _create_section_button = Gtk.Template.Child()
    _headers_view = Gtk.Template.Child()
    _h1_button = Gtk.Template.Child()
    _h2_button = Gtk.Template.Child()
    _h3_button = Gtk.Template.Child()
    _justifications_view = Gtk.Template.Child()
    _center_button = Gtk.Template.Child()
    _left_button = Gtk.Template.Child()
    _right_button = Gtk.Template.Child()
    _highlights_view = Gtk.Template.Child()
    _bold_button = Gtk.Template.Child()
    _strikethroug_button = Gtk.Template.Child()
    _underline_button = Gtk.Template.Child()
    _italic_button = Gtk.Template.Child()

    def __init__(self, buffer):

        super().__init__()       

        self.tag_manager = TagManager

        self._format_stack.set_transition_duration(500)

        self._header_button.connect("clicked",
                                    self._go_to_format,
                                    self._headers_view)

        self._justify_button.connect("clicked",
                                     self._go_to_format,
                                     self._justifications_view)

        self._highligh_button.connect("clicked",
                                      self._go_to_format,
                                      self._highlights_view)

        self._h1_button.connect("clicked", self._apply_format, "h1")
        self._center_button.connect("clicked", self._apply_format, "center")

        self._bold_button.connect("clicked", self._apply_format, "bold")
        self._italic_button.connect("clicked", self._apply_format, "italic")

        self._create_note_button.connect(
            "clicked", self._create_element, "note")

    def _go_to_format(self, button, view):

        self._format_stack.set_transition_type(5)
        self._format_stack.set_visible_child(view)

    def _apply_format(self, button, tag_name):

        self.tag_manager._apply_tag(tag_name, self.buffer)

        self._format_stack.set_transition_type(4)
        self._format_stack.set_visible_child(self._main_view)

    def _create_element(self, button, type):

        if (type == "note"):
            reference = str(datetime.datetime.now())
            name = "note:" + reference

            self.tag_manager._create_element_tag(name. self.buffer)
            self.tag_manager._apply_tag(name, self.buffer)

            #self.paned_box._notes_board._add_note(button, reference)

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/ShortcutsWindow.ui')
class ShortcutsWindow(Gtk.ShortcutsWindow):

    __gtype_name__ = 'ShortcutsWindow'

    def __init__(self):

        super().__init__()

import xml.etree.ElementTree as xmlet
import xmlformatter as xmlft
from diff_match_patch import diff_match_patch

# from .deserializer import deserializer_situation


def prepare_content(content: xmlet.Element) -> str:

    txt_content = xmlet.tostring(content, encoding="unicode")    

    formatter = xmlft.Formatter(preserve="<text>")  # normalize indentation
    frmt_content = (formatter.format_string(txt_content)).decode()    

    return frmt_content


def create_line_diff(frmt_actual_content: str,
                     frmt_saved_content: str) -> str:

    dmp = diff_match_patch()

    unicode_tuple = dmp.diff_linesToChars(frmt_actual_content,
                                          frmt_saved_content)

    lined_actual, lined_saved, lined_array = unicode_tuple

    diff = dmp.diff_main(lined_actual, lined_saved, False)
    dmp.diff_charsToLines(diff, lined_array)

    return diff


def create_patch(actual_content: xmlet.Element,
                 saved_content: xmlet.Element) -> str | None:

    dmp = diff_match_patch()

    frmt_actual_content = prepare_content(actual_content)
    frmt_saved_content = prepare_content(saved_content)

    diff = create_line_diff(frmt_actual_content, frmt_saved_content)

    if not (diff):
        return None

    patch_array = dmp.patch_make(frmt_actual_content, diff)
    patch = dmp.patch_toText(patch_array)

    return patch


def apply_patch(base_content: str,
                patch: str) -> str:

    dmp = diff_match_patch()

    # base_text = prepare_content(base_content)

    patch_array = dmp.patch_fromText(patch)
    result_text, _result = dmp.patch_apply(patch_array, base_content)

    return result_text
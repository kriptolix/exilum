# application.py
#
# Copyright 2023 k
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


import gi
import sys
from gi.repository import Gtk, Gio, Adw, GObject, GLib
from .gtk.widgets.mainwindow import MainWindow
from .gtk.widgets.messagedialog import MessageDialog

from .serializer import serialize_situation, serialize_to_plain_text
from .deserializer import deserialize_project, deserialize_situation
from .versioner import create_patch, prepare_content, apply_patch
from .composer import compose, compose_tmp
from .tests.testresources import notes_test, sections_test, versions_test
from .utils import create_action, update_recent_projects, SaveCountdown

from .iooperations import write_to_disk_async, load_from_disk_async, load_from_disk
from .iooperations import scan_for_tmp, set_file_monitor, remove_file

from .gtk.widgets.sectionnode import SectionObject

from datetime import datetime
import xml.etree.ElementTree as xmlet

# import debugpy
# debugpy.listen(('127.0.0.1', 9002))
# debugpy.wait_for_client()
# debugpy.breakpoint()

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')


class ExiliumApplication(Adw.Application):
    """The main application singleton class."""

    last_path_accessed = GObject.Property(type=str, default=None)
    tmp_saved_file = GObject.Property(type=object, default=None)
    last_loaded_file = GObject.Property(type=object, default=None)
    in_progress_content = GObject.Property(type=object, default=None)
    disc_loaded_content = GObject.Property(type=object, default=None)
    next_action = GObject.Property(type=bool, default=False)

    def __init__(self):
        super().__init__(application_id='io.gitlab.kriptolix.Exilium',
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)

        create_action(self, "app", 'about', self._on_about, None, None)

        create_action(self, "app", "quit", self._on_quit, ['<primary>q'],
                      GLib.VariantType.new("b"))

        create_action(self, "app", 'new',
                      self._menu_actions, ['<Ctrl>n'])
        create_action(self, "app", 'open',
                      self._menu_actions, ['<Ctrl>o'])
        create_action(self, "app", "versionate", self._versionate_project,
                      ["<Ctrl>s"], GLib.VariantType.new("s"))
        create_action(self, "app", 'export', self._export_project, None, None)

        self.save_countdown = SaveCountdown(3, self._autosave_project)

        self.settings = Gio.Settings(schema_id="io.gitlab.kriptolix.Exilium")

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        self._window = self.props.active_window
        if not self._window:
            self._window = MainWindow(self)

        self._window.connect("close-request", self._on_quit_button)

        self.sections_list = self._window._paned_box._sections_list
        self.sections_text = self._window._paned_box._sections_text
        self.versions_list = self._window._paned_box._versions_list
        self.paned_box = self._window._paned_box

        self.notes_board = self._window._paned_box._notes_board

        handler = self.versions_list._selection.connect("selection-changed",
                                                        self.change_version)

        self.versions_list.selection_handler = handler

        self._window._paned_box._home_screen.list_recent()

        self._window.present()

        # locale.setlocale(locale.LC_TIME, 'pt_BR.UTF-8')

        self._execute_tests()

    def _on_quit_button(self, window):

        self.activate_action("quit",
                             GLib.Variant.new_boolean(True))
        return True

    def _on_quit(self, action: Gio.SimpleAction,
                 parameter_type: GLib.VariantType) -> None:

        save = parameter_type.unpack()

        if (self.in_progress_content
                and self.tmp_saved_file and save):

            self.activate_action("versionate",
                                 GLib.Variant.new_string("quit"))
            return

        if (self.tmp_saved_file):
            remove_file(self.tmp_saved_file)

        self.quit()

    def _on_about(self, widget, _):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='Exilium',
                                application_icon='io.gitlab.kriptolix.Exilium',
                                developer_name='k',
                                version='0.0.1',
                                developers=['k'],
                                copyright='© 2023 k')
        about.present()

    def _menu_actions(self, action: Gio.SimpleAction, parameter):

        action_name = action.get_name()

        if not (self.in_progress_content == self.disc_loaded_content):

            self.activate_action("versionate",
                                 GLib.Variant.new_string(action_name))
            return

        match action_name:
            case "open":
                self.open_project(None, None)
            case "new":
                self._new_project()

    def _new_project(self):

        def _dialog_response(dialog: Adw.MessageDialog,
                             result: Gio.AsyncResult):

            response = dialog.choose_finish(result)

            if (response == "cancel"):
                return

            self._reset_state()

            entry = dialog.get_extra_child()
            buffer = entry.get_buffer()
            text = buffer.get_text()

            section = SectionObject(text)

            self.sections_list.add_section(section, None)

            identifier = "Last time saved: Never"

            self.versions_list._add_version("In progress version",
                                            identifier,
                                            "")

            self._window._on_project_opened()

            xml_situation = serialize_situation(self.sections_list,
                                                self.notes_board)

            self.in_progress_content = xml_situation

        ##

        if (self.in_progress_content and
                self.in_progress_content != self.disc_loaded_content):

            self.activate_action("versionate",
                                 GLib.Variant.new_string("new"))

            return

        dialog = MessageDialog(self._window, "create_project")
        dialog.choose(None, _dialog_response)

    def open_project(self, button, path):

        def when_loaded(gfile, content):

            if not (gfile):
                self.paned_box.show_toast(str(content.message))
                return

            set_file_monitor(gfile)

            self.last_loaded_file = gfile
            update_recent_projects(self.settings, gfile.get_path())

            xml_project = xmlet.fromstring(content)
            xml_situation = xml_project.find("situation")

            self.disc_loaded_content = xml_situation
            self.in_progress_content = xml_situation

            self._reset_state()

            deserialize_project(xml_project,
                                self.sections_list,
                                self.sections_text,
                                self.notes_board,
                                self.versions_list)

            self._window._on_project_opened()

        def _dialog_callback(dialog: Adw.MessageDialog,
                             result: Gio.AsyncResult) -> None:

            gfile = dialog.open_finish(result)
            load_from_disk_async(gfile, when_loaded)

        ##

        if (path):

            gfile = Gio.File.new_for_path(path)  # open dont ask place
            load_from_disk_async(gfile, when_loaded)
            return

        '''if not (self.in_progress_content == self.disc_loaded_content):

            self.activate_action("versionate", GLib.Variant.new_string("open"))
            return'''

        file_dialog = Gtk.FileDialog.new()
        file_dialog.open(None, None, _dialog_callback)

    def change_version(self,
                       selection_model: Gtk.SingleSelection,
                       position: int,
                       n_items: int) -> None:

        position = selection_model.get_selected()

        xml_situation = self.in_progress_content

        if (position != 0):

            situation = prepare_content(self.in_progress_content)

            list_store = self.versions_list._list_store

            for p in range(1, (position + 1)):

                version = list_store.get_item(p)

                if not (version.patch):
                    break

                situation = apply_patch(situation,
                                        version.patch)

            xml_situation = xmlet.fromstring(situation)

        self.sections_list._list_store.remove_all()
        self.sections_text._list_store.remove_all()
        self.notes_board._list_store.remove_all()

        deserialize_situation(xml_situation,
                              self.sections_list,
                              self.sections_text,
                              self.notes_board)

    def _reset_state(self):

        self.sections_list._list_store.remove_all()
        self.sections_text._list_store.remove_all()
        self.versions_list._list_store.remove_all()
        self.notes_board._list_store.remove_all()

        self.tmp_saved_file = None

    def _versionate_project(self, action: Gio.SimpleAction,
                            parameter_type: GLib.VariantType):

        def _next_action():

            match next_action:

                case "new":
                    self._new_project()
                    return

                case "open":
                    self.open_project(None, None)
                    return

                case "quit":
                    self.activate_action("quit",
                                         GLib.Variant.new_boolean(False))

        def when_writed(gfile, error):

            if (error):
                self.paned_box.show_toast(str(error.message))
                return

            update_recent_projects(self.settings, gfile.get_path())

            self.disc_loaded_content = self.in_progress_content

            _next_action()

        def _choose_dialog_callback(file_dialog: Gtk.FileDialog,
                                    task: Gio.AsyncResult,
                                    data: str):

            gfile = file_dialog.save_finish(task)

            self.last_loaded_file = gfile
            write_to_disk_async(gfile, data, when_writed)

        def _save_content():

            xml_situation = serialize_situation(self.sections_list,
                                                self.notes_board)

            self.in_progress_content = xml_situation

            if (self.disc_loaded_content):

                patch = create_patch(self.in_progress_content,
                                     self.disc_loaded_content)

                if not (patch):
                    return

                title = "New version"

            else:

                patch = ""
                title = "First version"

            date = datetime.now().strftime("%a %d %b %Y, %H:%M")
            self.versions_list._add_version(title, date, patch, 1)

            new_file_content = compose(self.versions_list, xml_situation)

            if (self.last_loaded_file):

                write_to_disk_async(self.last_loaded_file,
                                    new_file_content,
                                    when_writed)

            else:
                file_dialog = Gtk.FileDialog.new()
                file_dialog.set_title("Save File")
                # file_dialog.set_initial_folder(last_directory)
                file_dialog.save(None, None,
                                 _choose_dialog_callback,
                                 new_file_content)

        def _save_dialog_response(dialog: Adw.MessageDialog,
                                  result: Gio.AsyncResult):

            response = dialog.choose_finish(result)

            if (response == "cancel"):
                return

            if (response == "discard"):
                _next_action()
                return

            _save_content()

    ##
        next_action = parameter_type.unpack()

        if (next_action == "void"):
            _save_content()

            return

        dialog = MessageDialog(self._window, "save_project")
        dialog.choose(None, _save_dialog_response)

    def _autosave_project(self, *args):

        def when_writed(gfile, error):

            if (error):
                self.paned_box.show_toast(str(error.message))
                return

            self.versions_list.update_last_save()

        ##

        xml_situation = serialize_situation(self.sections_list,
                                            self.notes_board)

        if (self.in_progress_content == xml_situation):
            return

        self.in_progress_content = xml_situation

        if not (self.tmp_saved_file):

            row = self.sections_list._model.get_row(0)
            node_object = row.get_item()
            title = "project-" + node_object.title
            name = title + datetime.now().strftime("-%Y%m%d%H%M%S")

            path = GLib.get_user_cache_dir() + "/" + name

            self.tmp_saved_file = Gio.File.new_for_path(path)

        path = ""

        if (self.last_loaded_file):
            path = self.last_loaded_file.get_path()

        new_file_content = compose_tmp(xml_situation, path)

        write_to_disk_async(self.tmp_saved_file,
                            new_file_content,
                            when_writed)

    ###

    def _export_project(self, action: Gio.SimpleAction, parameter):

        def when_writed(gfile, error):

            if (error):
                self.paned_box.show_toast(str(error.message))
                return

            self.paned_box.show_toast("file exported")

        def _choose_dialog_callback(file_dialog: Gtk.FileDialog,
                                    task: Gio.AsyncResult,
                                    data: str):

            try:
                gfile = file_dialog.save_finish(task)

            except GLib.GError as error:

                print(str(error.message))
                return

            self.last_loaded_file = gfile
            write_to_disk_async(gfile, data, when_writed)

        ##

        plain_text = serialize_to_plain_text(self.sections_list)

        file_dialog = Gtk.FileDialog.new()
        file_dialog.set_title("Export File")
        # file_dialog.set_initial_folder(last_directory)
        file_dialog.save(None, None,
                         _choose_dialog_callback,
                         plain_text)

    def _recover_unsaved(self, *args):

        def load_file(content):

            xml_tmp_project = xmlet.fromstring(content)

            xml_file = xml_tmp_project.find("path")
            nonlocal xml_tmp_situation
            xml_tmp_situation = xml_tmp_project.find("situation")

            file_path = xml_file.text

            if (file_path):

                original_file = Gio.File.new_for_path(file_path)
                self.last_loaded_file = original_file

                load_from_disk_async(original_file, when_loaded)

            deserialize_situation(xml_tmp_situation,
                                  self.sections_list,
                                  self.sections_text,
                                  self.notes_board)

            self.tmp_saved_file = gfile

            self._window._on_project_opened()

        def when_loaded(gfile, content):

            if not (gfile):
                self.paned_box.show_toast(str(content.message))
                return

            file_name = gfile.get_basename()

            if file_name.startswith("project"):
                load_file(content)
                return

        def _dialog_response(dialog: Adw.MessageDialog,
                             result: Gio.AsyncResult) -> None:

            response = dialog.choose_finish(result)

            if (response == "discard"):

                remove_file(gfile)

                return

            if (response == "open"):

                teste = load_from_disk(gfile)
                print(teste)

        ##

        recovered_list = scan_for_tmp()

        if (len(recovered_list) > 0):

            xml_tmp_situation = ""
            gfile = recovered_list[0]

            dialog = MessageDialog(self._window,
                                   "recover_unsaved",
                                   gfile)

            dialog.choose(None, _dialog_response)

    def reset_timer(self, *args):
        self.save_countdown.update_count()

    def _execute_tests(self):

        notes_test(self.notes_board)
        sections_test(self.sections_list)
        versions_test(self.versions_list)

        self._window._on_project_opened()

        xml_situation = serialize_situation(self.sections_list,
                                            self.notes_board)

        self.in_progress_content = xml_situation


def main(version):
    """The application's entry point."""
    app = ExiliumApplication()
    return app.run(sys.argv)

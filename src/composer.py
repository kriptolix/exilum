import xml.etree.ElementTree as xmlet
from datetime import datetime

from .serializer import serialize_versions


def compose(versions_list: object,
            situation: xmlet.Element):

    xml_project = xmlet.Element('project')

    xml_versions = serialize_versions(versions_list)

    xml_project.append(situation)
    xml_project.append(xml_versions)

    xmlet.indent(xml_project)

    text_content = (xmlet.tostring(xml_project, encoding="unicode",
                                   short_empty_elements=False))
    return text_content


def compose_tmp(situation: xmlet.Element, path: str):

    xml_project = xmlet.Element('project')

    xml_path = xmlet.SubElement(xml_project, "path")
    xml_path.text = path

    xml_project.append(situation)

    xmlet.indent(xml_project)

    text_content = (xmlet.tostring(xml_project, encoding="unicode",
                                   short_empty_elements=False))
    return text_content

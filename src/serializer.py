import xml.etree.ElementTree as xmlet
from datetime import datetime

from gi.repository import Gtk


def _serialize_text(buffer):

    if (isinstance(buffer, Gtk.TextBuffer)):
        start = buffer.get_start_iter()
        end = buffer.get_end_iter()
        text = buffer.get_text(start, end, False)

    if (isinstance(buffer, Gtk.EntryBuffer)):
        text = buffer.get_text()

    return text


def _tag_serializer(section, xml_section):

    buffer = section.text_buffer
    iterator = buffer.get_start_iter()

    offset = 0

    tag_map = []

    while not (iterator.is_end()):

        offset = iterator.get_offset()

        open_tags = iterator.get_toggled_tags(True)
        close_tags = iterator.get_toggled_tags(False)

        if (open_tags):
            for tag in open_tags:
                tag_map.append([tag.props.name, offset, 0])

        if (close_tags):
            for tag in close_tags:
                for item in tag_map:
                    if (item[0] == tag.props.name and
                            item[2] == 0):
                        item[2] = offset

        iterator.forward_to_tag_toggle()

    xml_tags = xmlet.SubElement(xml_section, "tags")
    if (tag_map):

        for tag in tag_map:

            xml_tag = xmlet.SubElement(xml_tags, "tag")
            xml_name = xmlet.SubElement(xml_tag, "name")
            xml_open = xmlet.SubElement(xml_tag, "open")
            xml_close = xmlet.SubElement(xml_tag, "close")

            xml_name.text = f"{tag[0]}"
            xml_open.text = f"{tag[1]}"
            xml_close.text = f"{tag[2]}"


def _sections_serializer(main_section):

    def _iterate_over(actual, xml_parent):

        if not (xml_parent):
            xml_parent = xml_sections

        xml_section = xmlet.SubElement(xml_parent, "section")
        xml_title = xmlet.SubElement(xml_section, "title")

        xml_title.text = f"{actual.title}"

        section_text = _serialize_text(actual.text_buffer)
        xml_text = xmlet.SubElement(xml_section, "text")
        xml_text.text = section_text

        _tag_serializer(actual, xml_section)

        if (actual.children):
            for child in actual.children:
                _iterate_over(child, xml_section)

    ##

    xml_sections = xmlet.Element("sections")

    _iterate_over(main_section, None)

    return xml_sections


def serialize_versions(versions_list):

    xml_versions = xmlet.Element("versions")

    versions_store = versions_list._list_store

    for version in versions_store:

        if (version.message == "In progress version"):
            continue

        xml_version = xmlet.SubElement(xml_versions, "version")

        xml_message = xmlet.SubElement(xml_version, "message")
        xml_identifier = xmlet.SubElement(xml_version, "identifier")
        xml_patch = xmlet.SubElement(xml_version, "patch")

        xml_message.text = version.message
        xml_identifier.text = version.identifier
        xml_patch.text = version.patch

    return xml_versions


def _serialize_notes(notes_board):

    notes_store = notes_board._list_store

    xml_notes = xmlet.Element("notes")

    for note in notes_store:

        note_situation = _serialize_text(note.content_buffer)
        note_title = _serialize_text(note.title_buffer)

        xml_note = xmlet.SubElement(xml_notes, "note")

        xml_title = xmlet.SubElement(xml_note, "title")
        xml_text = xmlet.SubElement(xml_note, "text")
        xml_tag = xmlet.SubElement(xml_note, "tag")
        xml_edge = xmlet.SubElement(xml_note, "edge")

        xml_title.text = note_title
        xml_text.text = note_situation
        xml_tag.text = note.tag
        xml_edge.text = note.edge

    return xml_notes


def serialize_situation(sections_list, notes_board):

    row = sections_list._model.get_row(0)
    root_object = row.get_item()

    xml_situation = xmlet.Element("situation")
    xml_project_title = xmlet.SubElement(xml_situation, "project_title")
    xml_project_title.text = f"{root_object.title}"

    xml_sections = _sections_serializer(root_object)
    xml_notes = _serialize_notes(notes_board)

    xml_situation.append(xml_sections)
    xml_situation.append(xml_notes)

    return xml_situation


def serialize_to_plain_text(sections_list):

    def _iterate_over(actual):

        nonlocal plain_text

        section_text = _serialize_text(actual.text_buffer)

        plain_text = plain_text + '\n\n' + section_text

        if (actual.children):
            for child in actual.children:
                _iterate_over(child)

    ##

    row = sections_list._model.get_row(0)
    root_object = row.get_item()

    plain_text = ""

    _iterate_over(root_object)

    return plain_text

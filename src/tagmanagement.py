from gi.repository import Gtk, Pango

# color = Gdk.RGBA()
# color.parse('#7F7F7F')
# color.to_string()  # 'rgb(127,127,127)'
# self._buffer.create_tag('search_highlight', background="yellow")


def initialize_tags(tag_table):

    format_tags = [
        ["italic", "style", Pango.Style.ITALIC],
        ["bold", "weight", 800],
        ["search_highlight", "background", "yellow"]
    ]

    for tag in format_tags:
        new = Gtk.TextTag.new(tag[0])
        setattr(new, tag[1], tag[2])
        tag_table.add(new)

    test = Gtk.TextTag.new("test")
    test.background = "yellow"
    tag_table.add(test)


def create_tag(tag_name, tag_table):

    tag = Gtk.TextTag.new(tag_name)
    tag_table.add(tag)


def create_format_tag(buffer: Gtk.TextBuffer) -> None:

    buffer.create_tag("search_highlight", background="green")


def apply_format_tag(name: str,
                     buffer: Gtk.TextBuffer) -> None:

    selection = buffer.get_selection_bounds()

    # mark 0
    if (selection[0].ends_word()):

        selection[0].forward_visible_word_end()
        selection[0].backward_visible_word_start()

    elif (selection[0].inside_word()):
        selection[0].backward_visible_word_start()

    # mark 1
    if (selection[1].starts_word()):

        selection[1].backward_visible_word_start()
        selection[1].forward_visible_word_end()

    elif (selection[1].inside_word()):
        selection[1].forward_visible_word_end()

    buffer.apply_tag_by_name(name, selection[0], selection[1])


def exclude_tag(self, name: str) -> None:

    tag = self._tag_table.lookup(name)
    self._tag_table.remove(tag)


def remove_tag(buffer: Gtk.TextBuffer,
               name: str,
               start: int,
               end: int) -> None:

    buffer.remove_tag_by_name(name, start, end)

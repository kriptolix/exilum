from gi.repository import Gtk

from .themeselector import ThemeSelector


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/ApplicationMenu.ui')
class ApplicationMenu(Gtk.PopoverMenu):

    __gtype_name__ = 'ApplicationMenu'

    def __init__(self):

        super().__init__()

        self._theme_selector = ThemeSelector()
        self.add_child(self._theme_selector, "theme_selector")


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/SectionsMenu.ui')
class SectionsMenu(Gtk.PopoverMenu):

    __gtype_name__ = 'SectionsMenu'

    def __init__(self):

        super().__init__()
from gi.repository import Gtk, Gio, Adw, GObject, Gdk


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/ThemeSelector.ui')
class ThemeSelector(Gtk.Box):

    __gtype_name__ = 'ThemeSelector'

    @GObject.Property(type=str)
    def selected_color_scheme(self):
        """Read-write integer property."""

        return self.color_scheme

    @selected_color_scheme.setter
    def selected_color_scheme(self, color_scheme):
        self.color_scheme = color_scheme

        if color_scheme == "auto":
            self._system_selector.set_active(True)
            self.style_manager.set_color_scheme(Adw.ColorScheme.PREFER_LIGHT)
        if color_scheme == "light":
            self._light_selector.set_active(True)
            self.style_manager.set_color_scheme(Adw.ColorScheme.FORCE_LIGHT)
        if color_scheme == "dark":
            self._dark_selector.set_active(True)
            self.style_manager.set_color_scheme(Adw.ColorScheme.FORCE_DARK)

    font_size = GObject.Property(type=int, default=11)
    font_css_provider = GObject.Property(type=object, default=None)

    _system_selector = Gtk.Template.Child()
    _light_selector = Gtk.Template.Child()
    _dark_selector = Gtk.Template.Child()
    _zoom_out_button = Gtk.Template.Child()
    _percent_label = Gtk.Template.Child()
    _zoom_in_button = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self.settings = Gio.Settings(schema_id="io.gitlab.kriptolix.Exilium")

        self.color_scheme = self.settings.get_string("color-scheme")
        self.style_manager = Adw.StyleManager.get_default()

        self._system_selector.set_active(True)

        self.settings.bind(
            "color-scheme",
            self,
            "selected_color_scheme",
            Gio.SettingsBindFlags.DEFAULT)

        self._system_selector.connect("toggled", self._on_color_scheme_changed)
        self._light_selector.connect("toggled", self._on_color_scheme_changed)
        self._dark_selector.connect("toggled", self._on_color_scheme_changed)

        self._zoom_in_button.connect("clicked", self._change_font_size)
        self._zoom_out_button.connect("clicked", self._change_font_size)

    def _on_color_scheme_changed(self, widget):
        if self._system_selector.get_active():
            self.selected_color_scheme = "auto"
        if self._light_selector.get_active():
            self.selected_color_scheme = "light"
        if self._dark_selector.get_active():
            self.selected_color_scheme = "dark"

    def _change_font_size(self, button):

        if button == self._zoom_out_button:
            self.font_size = self.font_size - 1

        if button == self._zoom_in_button:
            self.font_size = self.font_size + 1

        css = f'textview {{font-size: {self.font_size}pt;}}'

        new_font_css_provider = Gtk.CssProvider()
        new_font_css_provider.load_from_string(css)        

        if self.font_css_provider:
            Gtk.StyleContext.remove_provider_for_display(Gdk.Display.get_default(),
                                                         self.font_css_provider)

        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(),
                                                  new_font_css_provider,
                                                  Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        percent = round((self.font_size / 11) * 100)
        self._percent_label.set_text(f'{percent}%')

        self.font_css_provider = new_font_css_provider

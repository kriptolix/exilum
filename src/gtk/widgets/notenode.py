from gi.repository import GObject, Gtk

from ...utils import create_click


class NoteObject(GObject.Object):  # Node Object

    __gtype_name__ = 'NoteObject'

    title_buffer = GObject.Property(type=object, default=None)
    content_buffer = GObject.Property(type=object, default=None)
    tag = GObject.Property(type=str, default=None)
    edge = GObject.Property(type=str, default="green")

    def __init__(self):

        super().__init__()

        self.title_buffer = Gtk.EntryBuffer.new(None, -1)
        self.content_buffer = Gtk.TextBuffer.new()


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/NoteWidget.ui')
class NoteWidget(Gtk.Frame):  # Note Widget

    __gtype_name__ = 'NoteWidget'

    notes_board = GObject.Property(type=object, default=None)
    note_object = GObject.Property(type=object, default=None)

    _title = Gtk.Template.Child()
    _content = Gtk.Template.Child()
    _edge = Gtk.Template.Child()
    _color_button = Gtk.Template.Child()
    _close_button = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._content.set_wrap_mode(3)
        self._content.set_left_margin(30)
        self._content.set_right_margin(15)
        self._content.set_vexpand(True)
        self._content.set_hexpand(True)        

        create_click(self._content, 1, "released", self._on_click)

        self._colors = ["green", "red", "yellow", "blue"]

        self._close_button.connect("clicked", self._remove_note)
        self._color_button.connect("clicked", self._change_color)

        self.over_mouse = Gtk.EventControllerMotion.new()

        self.over_mouse.connect("enter", self._on_over_mouse)
        self.over_mouse.connect("leave", self._on_over_mouse)

        self.add_controller(self.over_mouse)

    def _remove_note(self, _) -> None:

        self.notes_board._remove_note(self.note_object)

    def _change_color(self, _,
                      color: str | None = None) -> None:

        if not (color):
            index = self._colors.index(self.note_object.edge)

            old_color = self._colors[index]

            self._edge.remove_css_class(f"edge-{old_color}")
            self._color_button.remove_css_class(f"button-{old_color}")

            match index:
                case 0:
                    color = self._colors[1]
                case 1:
                    color = self._colors[2]
                case 2:
                    color = self._colors[3]
                case 3:
                    color = self._colors[0]

        self._edge.add_css_class(f"edge-{color}")
        self._color_button.add_css_class(f"button-{color}")
        self.note_object.edge = color

    def _on_over_mouse(self, _,
                       x: int | None = None,
                       y: int | None = None) -> None:

        if (x and y):
            self._color_button.set_opacity(1)
            self._close_button.set_opacity(1)
            return

        self._color_button.set_opacity(0)
        self._close_button.set_opacity(0)

    def _on_click(self, *args):
        '''
        Ensures that the TextView will grab focus
        '''

        self._content.grab_focus()

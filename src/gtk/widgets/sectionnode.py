from gi.repository import GObject, Gtk, Gdk

from .menus import SectionsMenu
from ...utils import create_click
# from ...spellchecker import Spellchecker

class SectionObject(GObject.Object):  # Node Object

    __gtype_name__ = 'SectionObject'

    title = GObject.Property(type=str, default=None)
    parent = GObject.Property(type=object, default=None)
    text_buffer = GObject.Property(type=object, default=None)
    expanded = GObject.Property(type=bool, default=False)
    row = GObject.Property(type=object, default=None)

    def __init__(self, title):

        super().__init__()

        self.title = title       
        self.children = []

    def _setup_change(self) -> None:

        if (self.row):
            self.expanded = self.row.get_expanded()

        if (len(self.children) > 0):
            for child in self.children:
                child._setup_change()

    def _on_item_change(self) -> None:

        self.row.set_expanded(True)
        self.expanded = False
        if (len(self.children) > 0):
            for child in self.children:
                if (child.expanded):
                    child._on_item_change()

    def add_child(self, child, position):

        if (position >= 0):

            self.children.insert(position, child)
            # print("insert")
            child.parent = self

            return

        self.children.append(child)        
        child.parent = self

    def get_relative_position(self):

        if not self.parent:
            return

        position = self.parent.children.index(self)

        return position


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/SectionWidget.ui')
class SectionWidget(Gtk.Box):  # Node Widget

    __gtype_name__ = 'SectionWidget'

    _title = Gtk.Template.Child()
    _icon = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._title.connect("notify::editing", self._on_exit_edit_mode)

        self._menu = SectionsMenu()
        self._menu.set_parent(self)

    def _active_edit_mode(self, event, key,
                          keycode, state) -> None:

        if (key == 65471):

            self._title.props.editing = True
            self._title.props.sensitive = True
            self._title.grab_focus()

    def _on_exit_edit_mode(self, klass, value) -> None:

        editing = self._title.props.editing
        if not (editing):
            self._title.props.sensitive = False


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/SectionDnDWidget.ui')
class SectionDnDWidget(Gtk.Box):  # Drag and Drop Widget

    __gtype_name__ = 'SectionDnDWidget'

    _title = Gtk.Template.Child()

    def __init__(self, expander):

        super().__init__()

        node_widget = expander.get_child()

        text = node_widget._title.props.text
        self._title.set_text(text)

        allocation = expander.get_allocation()
        self.set_size_request(allocation.width, allocation.height)

        tree_list_row = expander.get_list_row()
        depth = tree_list_row.get_depth()
        expandable = tree_list_row.is_expandable()

        for level in range(depth):
            expander = Gtk.Expander.new()
            self.prepend(expander)
            expander.set_opacity(0)

            if (level == depth - 1 and
                    expandable):
                expander.set_opacity(1)


class BufferObject(Gtk.TextBuffer):  # Buffer Object

    __gtype_name__ = 'BufferObject'

    section = GObject.Property(type=object, default=None)
    focused = GObject.Property(type=bool, default=False)

    def __init__(self, section, tag_table):

        super().__init__(tag_table=tag_table)

        self.section = section 

        # self.connect("notify::focused", lambda *_: print(self.focused))


class ViewWidget(Gtk.TextView):  # View Widget

    __gtype_name__ = 'ViewWidget'

    def __init__(self):

        super().__init__()

        self.set_wrap_mode(3)
        self.set_left_margin(80)
        self.set_right_margin(80)
        self.set_vexpand(True)
        self.set_hexpand(True)
        # self.set_size_request(-1, 20)

        self.buffer = self.get_buffer()

        # self.spellchcker = Spellchecker(self)
        # self.spellchcker.set_enabled(True)

        # self.connect("notify::has-focus", lambda *_: print(self.has_focus()))

        '''
        self._popover = None
        self._rectangle = None
        self._motion = Gtk.EventControllerMotion.new()
        self._motion.connect('motion', self.on_mouse_move)
        self.add_controller(self._motion)

        left_click = Gtk.GestureClick.new()
        left_click.set_button(1)
        left_click.connect("unpaired-release", self._on_left_click)        
        self.add_controller(left_click)        

        self._click = Gtk.GestureClick.new()
        self._click.set_button(1)
        self._click.connect("released", self._on_click)
        self.add_controller(self._click)'''

    def _focused(self, widget, propertie):

        if propertie:
            self.grab_focus()

    def _on_mark_set(self,
                     buffer: Gtk.TextBuffer,
                     location: Gtk.TextIter,
                     mark: Gtk.TextMark) -> None:

        print("mark seted")
        # self.scroll_mark_onscreen(mark)
    
    def _first(self, propertie):

        if propertie:
            self.set_top_margin(80)
        else:
            self.set_top_margin(15)

    def on_mouse_move(self, motion, x, y):

        def _on_init():

            self._popover = Gtk.Popover.new()
            self._popover.set_child(self.format_bar)
            self._popover.set_parent(self)
            self._popover.set_position(2)
            self._popover.set_has_arrow(False)

        ##

        if not (self.buffer.get_has_selection()):
            # print("No selection")
            return

        if not (self._rectangle):
            # print("No rectangle")
            return

        if not (self._popover):
            # self.format_bar = FormatBar(self.buffer)
            _on_init()

        wx, wy = self.window_to_buffer_coords(2, x, y)

        rx, ry, rw, rh = self._rectangle

        if (rx <= wx <= rw and ry <= wy <= rh):

            self._popover.set_offset(rx, ry + 30)
            self._popover.set_pointing_to(Gdk.Rectangle(rx, ry, 1, 1))
            self._popover.popup()
        else:
            self._popover.popdown()
            self.format_bar._go_to_format("b", self.format_bar._main_view)

    def _on_left_click(self,
                       event: Gtk.GestureClick,
                       t, q, w, e) -> None:

        print("left_click")

        if (self.buffer.get_has_selection()):

            start, end = self.buffer.get_selection_bounds()
            start_rectangle = self.get_iter_location(start)
            end_rectangle = self.get_iter_location(end)

            width = (end_rectangle.x + end_rectangle.width) - start_rectangle.x
            height = (end_rectangle.y + end_rectangle.height) - \
                start_rectangle.y

            self._rectangle = [start_rectangle.x,
                               start_rectangle.y,
                               start_rectangle.x + width,
                               start_rectangle.y + height]
        else:
            self._rectangle = []

    def _on_section_selected(self, widget, value, other):
        self.grab_focus()

    def _on_click(self, widget, event, x, y):  # clickable working

        con_x, con_y = self.window_to_buffer_coords(2, x, y)

        result, iter = self.get_iter_at_location(con_x, con_y)

        tags = iter.get_tags()

        print(iter.get_offset())

        for tag in tags:
            if ((tag.props.name)[0:4] == "note"):
                print((tag.props.name)[5:-1])

            print(tag.props.name)

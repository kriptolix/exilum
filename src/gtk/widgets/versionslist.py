from gi.repository import Gtk, Gio, GObject

from .versionnode import VersionWidget, VersoinObject
from typing import Any
from .messagedialog import MessageDialog
from gettext import gettext as _

from datetime import datetime


@Gtk.Template(resource_path='/io/gitlab/kriptolix/'
              'Exilium/src/gtk/ui/VersionsList.ui')
class VersionsList(Gtk.Box):

    __gtype_name__ = 'VersionsList'

    _list_view = Gtk.Template.Child()
    _new_button = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._last_selected = None
        self.selection_handler = None

        self._in_progress = None

        self._list_item_factory = Gtk.SignalListItemFactory()
        self._list_item_factory.connect("setup", self._on_setup)
        self._list_item_factory.connect("bind", self._on_bind)

        self._list_store = Gio.ListStore.new(item_type=VersoinObject)

        self._selection = Gtk.SingleSelection.new()
        self._selection.set_model(self._list_store)

        self._list_view.set_model(self._selection)
        self._list_view.set_factory(self._list_item_factory)

        self._new_button.set_detailed_action_name("app.versionate::void")

    def _on_setup(self,
                  factory: Gtk.SignalListItemFactory,
                  item: Gtk.ListItem) -> None:
        """Setup the widget to show in the Gtk.Listview"""

        version = VersionWidget()
        item.set_child(version)

    def _on_bind(self,
                 factory: Gtk.SignalListItemFactory,
                 item: Gtk.ListItem) -> None:
        """bind data from the NodeObject to the visibel widget"""

        version_widget = item.get_child()
        version_object = item.get_item()

        version_widget._title.set_text(version_object.message)
        version_widget._identifier.set_text(version_object.identifier)

        if (version_object.message == "In progress version"):
            self._in_progress = version_widget

        version_widget._title.bind_property("text", version_object, "message")

    def _add_version(self, message, identifier, patch, position=0):

        revision = VersoinObject(message, identifier, patch)

        if (len(self._list_store) == 0):
            self._list_store.append(revision)

        else:
            self._list_store.insert(position, revision)

        # Change selection to new row without trigger on_change_selection
        self._selection.handler_block(self.selection_handler)
        self._selection.set_selected(0)
        self._selection.handler_unblock(self.selection_handler)

    def update_last_save(self):

        identifier = "Last time saved: " + datetime.now().strftime("%H:%M")
        self._in_progress._identifier.set_text(identifier)

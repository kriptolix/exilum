from gi.repository import GObject, Gtk


class VersoinObject(GObject.Object):  # Node Object

    __gtype_name__ = 'VersionObject'

    message = GObject.Property(type=str, default=None)
    identifier = GObject.Property(type=str, default=None)
    patch = GObject.Property(type=str, default=None)

    def __init__(self, message, identifier, patch):

        super().__init__()

        self.message = message
        self.identifier = identifier
        self.patch = patch


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/VersionWidget.ui')
class VersionWidget(Gtk.Box):  # Note Widget

    __gtype_name__ = 'VersionWidget'

    _title = Gtk.Template.Child()
    _identifier = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        '''self._undo_button.connect("clicked", self._on_undo_clicked)

        mouse_motion = Gtk.EventControllerMotion.new()

        mouse_motion.connect("enter",
                             lambda *_: self._undo_button.set_visible(True))
        mouse_motion.connect("leave",
                             lambda *_: self._undo_button.set_visible(False))

        self.add_controller(mouse_motion)'''

    def _on_undo_clicked(self, button):
        print("clicked")

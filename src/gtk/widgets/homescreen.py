from gi.repository import Gtk, Gio


@Gtk.Template(resource_path='/io/gitlab/kriptolix/'
              'Exilium/src/gtk/ui/HomeScreen.ui')
class HomeScreen(Gtk.Box):

    __gtype_name__ = 'HomeScreen'

    _new_button = Gtk.Template.Child()
    _open_button = Gtk.Template.Child()
    _recent_list = Gtk.Template.Child()    

    def __init__(self):

        super().__init__(self)

        self.settings = Gio.Settings(schema_id="io.gitlab.kriptolix.Exilium")

        self.recent_projects = self.settings.get_strv("recent-projects")

        self._new_button.set_action_name("app.new")
        self._open_button.set_action_name("app.open")

    def list_recent(self):

        window = self.get_root()
        application = window.application
        open_project = application.open_project

        for path in self.recent_projects:

            button = Gtk.Button.new()
            label = Gtk.Label.new(path)
            button.set_child(label)
            button.add_css_class("flat")
            button.set_tooltip_text(path)

            label.set_halign(1)  # GTK_ALIGN_START
            label.set_max_width_chars(25)
            label.set_ellipsize(1)  # PANGO_ELLIPSIZE_START

            button.connect("clicked", open_project, path)

            self._recent_list.append(button)

from gi.repository import Gtk, Gio, Adw

from .notenode import NoteObject, NoteWidget

from .messagedialog import MessageDialog
from .statuspage import StatusPage

from gettext import gettext as _
from typing import Any


@Gtk.Template(resource_path='/io/gitlab/kriptolix/'
              'Exilium/src/gtk/ui/NotesBoard.ui')
class NotesBoard(Gtk.Box):

    __gtype_name__ = 'NotesBoard'

    _grid_view = Gtk.Template.Child()
    _new_button = Gtk.Template.Child()
    _note_stack = Gtk.Template.Child()
    _status_page = Gtk.Template.Child()
    _scrolled = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._list_store = Gio.ListStore.new(item_type=NoteObject)

        self.reset_timer = None

        self._list_item_factory = Gtk.SignalListItemFactory()
        self._list_item_factory.connect("setup", self._on_setup)
        self._list_item_factory.connect("bind", self._on_bind)

        self._filter = Gtk.CustomFilter.new(self._do_filter)
        self._model = Gtk.FilterListModel.new(self._list_store, self._filter)
        self._model.set_incremental(True)

        self._selection = Gtk.NoSelection.new()
        self._selection.set_model(self._model)

        self._grid_view.set_model(self._selection)
        self._grid_view.set_factory(self._list_item_factory)

        self._new_button.connect("clicked", self._add_note, None)

        title = _("Nothing Here Yet")
        description = _("Use the '+' button to add a note")
        icon = "info-symbolic"

        self._status_page.set_status(title, description, icon)

        self._list_store.connect("items-changed", self._on_no_items)
        self._list_store.items_changed(0, 0, 0)

    def _on_setup(self,
                  factory: Gtk.SignalListItemFactory,
                  item: Gtk.ListItem) -> None:
        """Setup the widget to show in the Gtk.Listview"""

        note = NoteWidget()
        item.set_child(note)

    def _on_bind(self,
                 factory: Gtk.SignalListItemFactory,
                 item: Gtk.ListItem) -> None:
        """bind data from the NodeObject to the visibel widget"""

        note_widget = item.get_child()
        note_object = item.get_item()

        note_widget.notes_board = self
        note_widget.note_object = note_object

        note_widget._title.set_buffer(note_object.title_buffer)
        note_widget._content.set_buffer(note_object.content_buffer)
        note_widget._change_color(None, note_object.edge)

        window = self.get_root()
        application = window.application
        self.reset_timer = application.reset_timer

        note_object.content_buffer.connect("delete-range",
                                           self.reset_timer)
        note_object.content_buffer.connect("insert-text",
                                           self.reset_timer)
        note_object.title_buffer.connect("deleted-text",
                                         self.reset_timer)
        note_object.title_buffer.connect("inserted-text",
                                         self.reset_timer)
        self._list_store.connect("items_changed",
                                 self.reset_timer)

    def _add_note(self,
                  button: Gtk.Button | None,
                  data: Any) -> None:

        note = NoteObject()

        if (data):  # programmatically

            title, text, tag = data

            if (title):
                note.title_buffer.insert_text(0, title, -1)

            note.content_buffer.set_text(text)
            note.tag = tag        
        
        self._list_store.insert(0, note)
        
        self._grid_view.scroll_to(0, Gtk.ListScrollFlags.NONE)

    def _remove_note(self, note: NoteObject) -> None:

        def _dialog_response(dialog: Adw.MessageDialog,
                             response: str) -> None:

            if (response == "cancel"):
                return

            _, position = self._list_store.find(note)

            self._list_store.remove(position)

        ##

        dialog = MessageDialog(self.get_root(), "remove_note")

        dialog.connect("response", _dialog_response)

    def _do_filter(self, filter):
        """This will be useful in the future"""
        return True

    def _on_no_items(self, *_) -> None:        
        
        items = self._list_store.get_n_items()

        if (items == 0):
            self._note_stack.set_visible_child(self._status_page)
        else:
            self._note_stack.set_visible_child(self._scrolled)

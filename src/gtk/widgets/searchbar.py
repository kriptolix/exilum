from gi.repository import Gtk, Adw, GObject

from ...tagmanagement import remove_tag

import re


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/SearchBar.ui')
class SearchBar(Adw.Bin):

    __gtype_name__ = 'SearchBar'

    replace_mode_enabled = GObject.property(type=bool, default=False)
    search_mode_enabled = GObject.property(type=bool, default=False)
    sections_text = GObject.property(type=object, default=None)

    _search_bar = Gtk.Template.Child()
    _search_entry = Gtk.Template.Child()
    _backward_button = Gtk.Template.Child()
    _forward_button = Gtk.Template.Child()
    _regex_toggle = Gtk.Template.Child()
    _case_toggle = Gtk.Template.Child()
    _replace_toggle = Gtk.Template.Child()
    _replace_entry = Gtk.Template.Child()
    _replace_revealer = Gtk.Template.Child()
    _replace_one = Gtk.Template.Child()
    _replace_all = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self.matches = []
        self.active = 0

        self.connect("notify::search-mode-enabled", self._search_enabled)
        self.connect("notify::replace-mode-enabled", self._replace_enabled)

        self._search_entry.connect("activate", self._search)
        self._search_entry.set_search_delay(500)
        self._search_entry.connect("search-changed", self._search)

        # self._forward_button.connect("clicked", self._scroll_forward)
        # self._backward_button.connect("clicked", self._scroll_backward)

    def _search_enabled(self, *args, **kwargs):

        if self.search_mode_enabled:

            for buffer in self.sections_text._list_store:
                if buffer.get_has_selection():

                    start, end = buffer.get_selection_bounds()
                    selection_text = buffer.get_slice(start,
                                                      end,
                                                      False)

                    self._search_entry.set_text(selection_text)
                    break

            self._search_entry.grab_focus()
            self._search_entry.select_region(0, -1)
            self._search()
        else:
            for buffer in self.sections_text._list_store:
                remove_tag(buffer,
                           "search_highlight",
                           buffer.get_start_iter(),
                           buffer.get_end_iter())

            self.matches = []
            self.replace_mode_enabled = False
            # self.focused_view.grab_focus()

    def _replace_enabled(self, _widget, _data):
        if self.replace_mode_enabled and not self.search_mode_enabled:
            self.search_mode_enabled = True

    def _search(self, _widget=None, _data=None, scroll=True):

        searchtext = self._search_entry.get_text()

        if not (searchtext):
            return

        self.matches = []
        self.active = 0

        for buffer in self.sections_text._visible_content:

            context_start = buffer.get_start_iter()
            context_end = buffer.get_end_iter()

            text = buffer.get_slice(context_start, context_end, False)

            buffer.remove_tag_by_name("search_highlight",
                                      context_start, context_end)

            # case sensitive?
            flags = False
            if not self._case_toggle.get_active():
                flags = flags | re.I

            # regex?
            if not self._regex_toggle.get_active():
                searchtext = re.escape(searchtext)

            buffer_matches = re.finditer(searchtext, text, flags)

            for match in buffer_matches:
                self.matches.append((buffer, match.start(), match.end()))
                start_iter = buffer.get_iter_at_offset(match.start())
                end_iter = buffer.get_iter_at_offset(match.end())
                buffer.apply_tag_by_name("search_highlight",
                                         start_iter, end_iter)

        if scroll:
            self._scroll_to(self.active)
        # LOGGER.debug(searchtext)

    def _scroll_to(self, index):

        if not self.matches:
            return

        self.active = index % len(self.matches)

        match = self.matches[self.active]
        buffer = match[0]
        start = match[1]
        end = match[2]

        start_iter = buffer.get_iter_at_offset(start)
        end_iter = buffer.get_iter_at_offset(end)

        # create a mark at the start of the coincidence to scroll to it
        # buffer.create_mark(None, start_iter, False)

        # select coincidence
        buffer.select_range(start_iter, end_iter)

    def replace_clicked(self, _widget, _data=None):
        self.replace(self.active)

    def replace_all(self, _widget=None, _data=None):
        with user_action(buffer):
            for match in reversed(self.matches):
                self.__do_replace(match)
        self.search(scroll=False)

    def replace(self, searchindex, _inloop=False):
        with user_action(buffer):
            self.__do_replace(self.matches[searchindex])
        active = self.active
        self.search(scroll=False)
        self.active = active
        self.scrollto(self.active)

    def __do_replace(self, match):
        start_iter = buffer.get_iter_at_offset(match[0])
        end_iter = buffer.get_iter_at_offset(match[1])
        buffer.delete(start_iter, end_iter)
        start_iter = buffer.get_iter_at_offset(match[0])
        buffer.insert(start_iter, self.replace_entry.get_text())

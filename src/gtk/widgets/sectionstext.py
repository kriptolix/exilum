from gi.repository import Gtk, Gio, GObject

from .sectionnode import BufferObject, ViewWidget

from ...tagmanagement import initialize_tags, create_format_tag


@Gtk.Template(resource_path='/io/gitlab/kriptolix/'
              'Exilium/src/gtk/ui/SectionsText.ui')
class SectionsText(Gtk.Box):

    __gtype_name__ = 'SectionsText'

    _view_list = Gtk.Template.Child()    

    def __init__(self):

        super().__init__()

        self._list_store = Gio.ListStore.new(item_type=BufferObject)

        self._list_item_factory = Gtk.SignalListItemFactory()
        self._list_item_factory.connect("setup", self._on_setup)
        self._list_item_factory.connect("bind", self._on_bind)

        self._filter_content = []
        self._visible_content = []

        self._filter = Gtk.CustomFilter.new(self._do_filter)
        self._model = Gtk.FilterListModel.new(self._list_store, self._filter)
        self._model.set_incremental(True)

        self._selection = Gtk.NoSelection.new()
        self._selection.set_model(self._model)

        self._view_list.set_model(self._selection)
        self._view_list.set_factory(self._list_item_factory)

        self._tag_table = Gtk.TextTagTable.new()
        # initialize_tags(self._tag_table)        

        self.reset_timer = None

    def create_text_view(self, position, section):

        text_buffer = section.text_buffer
        
        if not (section.text_buffer):
            
            text_buffer = BufferObject(section, self._tag_table)            
            section.text_buffer = text_buffer

        if (len(self._list_store) == 0):
            create_format_tag(text_buffer)
            self._list_store.append(text_buffer)

        else:
            self._list_store.insert(position, text_buffer)        

    def remove_text_view(self, sections):

        to_remove = []

        for item in self._list_store:

            if item.section in sections:
                to_remove.append(item)

        # Why this second For? I dont know why but remove itens directly
        # in the first For just dont work with certain section nodes

        for section in to_remove:
            _, position = self._list_store.find(section)
            self._list_store.remove(position)

    def _on_setup(self,
                  factory: Gtk.SignalListItemFactory,
                  item: Gtk.ListItem) -> None:
        """Setup the widget to show in the Gtk.Listview"""

        text_view = ViewWidget()
        item.set_child(text_view)

    def _on_bind(self,
                 factory: Gtk.SignalListItemFactory,
                 item: Gtk.ListItem) -> None:
        """bind data from the NodeObject to the visible widget"""

        text_view = item.get_child()
        text_buffer = item.get_item()

        text_view.set_buffer(text_buffer)
        text_view.set_size_request(-1, 20)

        text_view.connect("move-cursor", self._on_cursor_navigate)
        text_buffer.connect("notify::focused", text_view._focused)

        text_view.bind_property("has-focus", text_buffer, 
                                "focused", GObject.BindingFlags.DEFAULT)

        window = self.get_root()
        application = window.application
        self.reset_timer = application.reset_timer

        text_buffer.connect("delete-range", self.reset_timer)
        text_buffer.connect("insert-text", self.reset_timer)

        text_buffer.connect("mark-set", text_view._on_mark_set)

        size = len(self._visible_content)

        if (size > 0):
            index = self._visible_content.index(text_buffer)

            if (index == 0
                    or size == 0):
                text_view._first(True)
            else:
                text_view._first(False)
        else:
            text_view._first(True)

        text_view.grab_focus()

    def _do_filter(self, item):
        """Performs filtering to display only visible sessions"""

        n_content = len(self._filter_content)

        if (n_content == 0):
            return True

        if item.section in self._filter_content:
            self._visible_content.append(item)

            return True

    def process_filter(self, subsections):
        """Prepare the filter content"""

        self._filter_content = subsections
        self._visible_content = []

        self._filter.changed(Gtk.FilterChange.DIFFERENT)

    def _on_cursor_navigate(self, view, steptype, step, _):
        """Allows the cursor to move between different sessions (TextViews)"""

        buffer = view.get_buffer()
        insert_mark = buffer.get_insert()
        cursor = buffer.get_iter_at_mark(insert_mark)

        last = len(self._visible_content) - 1
        position = self._visible_content.index(buffer)

        if (position == 0):
            before = None
        else:
            before = self._visible_content[position - 1]

        if (position == last):
            after = None
        else:
            after = self._visible_content[position + 1]

        if (steptype == Gtk.MovementStep.VISUAL_POSITIONS):

            if (cursor.is_end() and step == 1):

                if (after):

                    buffer.focused = False
                    start = after.get_start_iter()
                    after.place_cursor(start)
                    after.focused = True

            if (cursor.is_start() and step == -1):

                if (before):
                    buffer.focused = False
                    end = before.get_end_iter()
                    before.place_cursor(end)
                    before.focused = True

        if (steptype == Gtk.MovementStep.DISPLAY_LINES):

            if (cursor.is_start() and step == -1):

                if (before):
                    buffer.focused = False
                    end = before.get_end_iter()
                    before.place_cursor(end)
                    before.focused = True

            if (cursor.is_end() and step == 1):

                if (after):

                    buffer.focused = False
                    start = after.get_start_iter()
                    after.place_cursor(start)
                    after.focused = True

    def _on_section_selected(self, *args):
        self.grab_focus()   

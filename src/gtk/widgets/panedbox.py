from gi.repository import Gtk, Adw

from .sectionslist import SectionsList
from .versionslist import VersionsList
from .notesboard import NotesBoard
from .homescreen import HomeScreen
from .sectionstext import SectionsText
from .searchbar import SearchBar


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/PanedBox.ui')
class PanedBox(Gtk.Box):

    __gtype_name__ = 'PanedBox'

    _paned = Gtk.Template.Child()
    _left_stack = Gtk.Template.Child()
    _sections_list = Gtk.Template.Child()
    _notes_board = Gtk.Template.Child()
    _versions_list = Gtk.Template.Child()
    _right_stack = Gtk.Template.Child()
    _sections_text = Gtk.Template.Child()
    _home_screen = Gtk.Template.Child()
    _edition_view = Gtk.Template.Child()
    _revealer_searchbar = Gtk.Template.Child()
    _searchbar = Gtk.Template.Child()
    _toast_overlay = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._sections_list.sections_text = self._sections_text
        self._searchbar.sections_text = self._sections_text  

        self.target_position = None
        self.on_animation = False

        self.sidebar_is_visible = False  # True
        self.target_position = 360  # self._paned.get_position()

        self._paned.set_position(0)  # 360
        self._paned.set_resize_start_child(False)

    def on_position_changed(self, widget, position):

        if not (self.on_animation):
            self.target_position = self._paned.get_position()

        print("position_changed: ", self._paned.get_position())

    def _toggle_panel(self, button):

        child_visible = self._left_stack.get_visible_child()

        match button.get_tooltip_text():
            case "Sections":
                if (self.sidebar_is_visible and
                        self._sections_list == child_visible):

                    self._hide_sidebar()
                    button.set_active(False)

                elif (self.sidebar_is_visible and
                        self._sections_list != child_visible):
                    self._left_stack.set_visible_child(self._sections_list)

                else:
                    self._left_stack.set_visible_child(self._sections_list)

                    self._show_sidebar()
                    button.set_active(True)

            case "Notes":
                if (self.sidebar_is_visible and
                        self._notes_board == child_visible):

                    self._hide_sidebar()
                    button.set_active(False)

                elif (self.sidebar_is_visible and
                        self._notes_board != child_visible):

                    # self.center.play()
                    self._left_stack.set_visible_child(self._notes_board)

                else:
                    self._left_stack.set_visible_child(self._notes_board)

                    self._show_sidebar()
                    button.set_active(True)

            case "Versions":
                if (self.sidebar_is_visible and
                        self._versions_list == child_visible):

                    self._hide_sidebar()
                    button.set_active(False)

                elif (self.sidebar_is_visible and
                        self._versions_list != child_visible):

                    # self.center.play()
                    self._left_stack.set_visible_child(self._versions_list)

                else:
                    self._left_stack.set_visible_child(self._versions_list)

                    self._show_sidebar()
                    button.set_active(True)

    def _setup_animation(self, start, end):

        target_property = Adw.PropertyAnimationTarget.new(self._paned,
                                                          "position")

        animation_timed = Adw.TimedAnimation.new(self._paned,
                                                 start,
                                                 end,
                                                 750,
                                                 target_property)

        animation_timed.set_easing(6)
        return animation_timed

    def _hide_sidebar(self):

        self.hide = self._setup_animation(self._paned.get_position(), 0)

        self.target_position = self._paned.get_position()
        self.on_animation = True
        self.hide.play()
        self.sidebar_is_visible = False
        self.on_animation = False

    def _show_sidebar(self):

        self.show = self._setup_animation(0, self.target_position)

        self.on_animation = True
        self.show.play()
        self.sidebar_is_visible = True
        self.on_animation = False

    def _toggle_search_replace(self, button):

        if (self._revealer_searchbar.get_reveal_child()):
            self._revealer_searchbar.set_reveal_child(False)
            self._searchbar.search_mode_enabled = False
        else:
            self._revealer_searchbar.set_reveal_child(True)
            self._searchbar.search_mode_enabled = True

    def show_toast(self, message):

        toast = Adw.Toast.new(message)

        self._toast_overlay.add_toast(toast)

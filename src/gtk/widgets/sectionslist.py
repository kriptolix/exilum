from gi.repository import Gtk, Gio, Gdk, GObject, Adw, GLib

from .sectionnode import SectionObject, SectionWidget, SectionDnDWidget
# from .menus import SectionsMenu
from .messagedialog import MessageDialog
from gettext import gettext as _

from ...utils import create_action, create_click

from typing import Any


@Gtk.Template(resource_path='/io/gitlab/kriptolix/'
              'Exilium/src/gtk/ui/SectionsList.ui')
class SectionsList(Gtk.Box):

    __gtype_name__ = 'SectionsList'

    sections_text = GObject.Property(type=object, default=None)
    changed = GObject.Property(type=bool, default=False)

    _list_view = Gtk.Template.Child()
    _new_button = Gtk.Template.Child()

    def __init__(self):

        super().__init__()

        self._list_item_factory = Gtk.SignalListItemFactory()
        self._list_item_factory.connect("setup", self._on_setup)
        self._list_item_factory.connect("bind", self._on_bind)

        self._list_store = Gio.ListStore.new(item_type=SectionObject)
        self._model = Gtk.TreeListModel.new(self._list_store,
                                            False,
                                            False,
                                            self._tree_list_callback)

        self._selection = Gtk.SingleSelection.new()
        self._selection.set_model(self._model)

        self._list_view.set_model(self._selection)
        self._list_view.set_factory(self._list_item_factory)

        self._selection.connect("selection-changed",
                                self._on_node_selected)

        self._action_group = Gio.SimpleActionGroup()
        self.insert_action_group("section", self._action_group)

        create_action(self._action_group, "section",
                      "add", self._action_add_section, None, None)
        create_action(self._action_group, "section",
                      "remove", self._action_remove_section, None, None)
        create_action(self._action_group, "section",
                      "rename", self._rename_section, None, None)

        self._new_button.set_action_name("section.add")

        self._drag_coords = [0, 0]

    def _tree_list_callback(self,
                            item: SectionObject) -> Gio.ListStore | None:
        """Create sub ListStores to save Node object children"""

        if not (item.children):
            return None

        store = Gio.ListStore.new(SectionObject)

        for child in item.children:
            store.append(child)

        return store

    def _on_setup(self,
                  factory: Gtk.SignalListItemFactory,
                  item: Gtk.ListItem) -> None:
        """Setup the widget to show in the Gtk.Listview"""

        widget = SectionWidget()
        expander = Gtk.TreeExpander.new()
        expander.set_child(widget)

        f2_press = Gtk.EventControllerKey.new()
        f2_press.connect('key-released', widget._active_edit_mode)

        expander.add_controller(f2_press)

        create_click(expander, 3, "released", self._on_right_click, item)

        drop_controller = Gtk.DropControllerMotion()
        drag_source = Gtk.DragSource(actions=Gdk.DragAction.MOVE)
        drop_target = Gtk.DropTarget.new(Gtk.TreeExpander, Gdk.DragAction.MOVE)

        expander.add_controller(drag_source)
        expander.add_controller(drop_target)
        expander.add_controller(drop_controller)

        drag_source.connect("prepare", self._on_prepare, expander)
        drag_source.connect("drag-begin", self._on_drag_begin, expander)

        # Update row visuals during DnD operation
        drop_controller.connect("enter", self._on_drop_hover)
        drop_controller.connect("leave", self._on_drop_hover)
        drop_controller.connect("motion", self._on_drop_hover)

        drop_target.connect("drop", self._on_drop, expander)

        item.set_child(expander)

    def _on_bind(self,
                 factory: Gtk.SignalListItemFactory,
                 item: Gtk.ListItem) -> None:
        """bind data from the NodeObject to the visible widget"""

        expander = item.get_child()
        node_widget = expander.get_child()

        row = item.get_item()
        expander.set_list_row(row)
        node_object = row.get_item()

        node_widget._title.props.text = node_object.title
        node_object.row = row
        node_object.widget = expander

        node_widget._title.bind_property("text", node_object, "title")

    def _action_add_section(self, action: Gio.SimpleAction,
                            parameter_type: GLib.VariantType) -> None:

        row = self._selection.get_selected_item()
        parent = row.get_item()
        title = _("New Section")
        section = SectionObject(title)

        self.add_section(section, parent)

    def add_section(self,
                    section: SectionObject,
                    parent: SectionObject | None,
                    section_position: int = -1) -> None:

        if not (parent):  # Main Section
            self._list_store.append(section)
            text_position = 0

        else:
            parent.add_child(section, section_position)

            text_position = self._get_absolute_position(section)

        self.sections_text.create_text_view(text_position, section)

        self._update_tree_list(parent)
        # self._rename_section(None, section)

        # Change selection to new row
        self._selection.set_selected(section.row.get_position())

    def _action_remove_section(self, action: Gio.SimpleAction,
                               parameter_type: GLib.VariantType) -> None:

        selected_row = self._selection.get_selected_item()
        self._remove_section(selected_row, False)

    def _remove_section(self, list_row: Gtk.TreeListRow,
                        silent: bool) -> None:

        def _remove():

            parent_section = parent_node.get_item()
            position = parent_node.get_position()

            subsections = self._get_subsections(section)
            self.sections_text.remove_text_view(subsections)

            parent_section.children.remove(section)

            self._selection.set_selected(position)
            self._update_tree_list()

        def _dialog_response(dialog: Adw.MessageDialog,
                             response: str) -> None:

            if (response == "cancel"):
                return

            _remove()

        ##

        section = list_row.get_item()
        parent_node = list_row.get_parent()

        if not (parent_node):
            return

        if (silent):
            _remove()

            return

        dialog = MessageDialog(self.get_root(), "remove_section")

        dialog.connect("response", _dialog_response)

    def _rename_section(self,
                        action: Gio.SimpleAction | None = None,
                        data: SectionObject | None = None) -> None:

        if (data):
            title = data.widget.get_child()._title
        else:
            row = self._selection.get_selected_item()
            node_object = row.get_item()
            title = node_object.widget.get_child()._title

        title.props.editing = True
        title.props.sensitive = True
        title.grab_focus()

    def _update_tree_list(self,
                          parent: SectionObject | None = None) -> None:

        # Save all rows actual status
        row = self._model.get_row(0)
        node_object = row.get_item()
        node_object._setup_change()

        # If are a insertion, set row who will gain child as expanded
        if (parent):
            parent.expanded = True

        # fake a ListoStore change to force refresh
        self._list_store.items_changed(0, 1, 1)

        # Re applying previous status on rows
        row = self._model.get_row(0)
        node_object = row.get_item()
        node_object._on_item_change()

    def _on_node_selected(self,
                          selection_model: Gtk.SingleSelection,
                          position: int,
                          n_items: int) -> None:

        selected_node = self._selection.get_selected_item()
        section = selected_node.get_item()

        subsections = self._get_subsections(section)
        self.sections_text.process_filter(subsections)

        # print("subsections ", subsections)

    def _get_subsections(self,
                         section: SectionObject) -> list:

        subsections = []

        def _get_tree(section):

            subsections.append(section)

            for child in section.children:
                _get_tree(child)
        ##

        _get_tree(section)

        return subsections

    def _get_absolute_position(self, section):

        main_node = self._model.get_row(0)
        main_section = main_node.get_item()

        sections = self._get_subsections(main_section)

        position = sections.index(section)

        return position

    def _on_right_click(self,
                        event: Gtk.GestureClick,
                        n_pres: int,
                        x: int,
                        y: int,
                        item) -> None:

        position = item.get_position()

        self._selection.set_selected(position)

        expander = item.get_child()
        widget = expander.get_child()

        widget._menu.set_offset(x, y)
        widget._menu.set_pointing_to(Gdk.Rectangle(x, y, 1, 1))
        widget._menu.popup()

    def _on_prepare(self, source, x, y, expander):

        self._drag_coords[0] = x
        self._drag_coords[1] = y

        value = GObject.Value()
        value.init(Gtk.TreeExpander)
        value.set_object(expander)

        return Gdk.ContentProvider.new_for_value(value)

    def _on_drag_begin(self, _source, drag_object, expander):

        tree_list_row = expander.get_list_row()
        position = tree_list_row.get_position()
        self._selection.set_selected(position)

        if (tree_list_row.get_expanded()):
            tree_list_row.set_expanded(False)

        drag_widget = SectionDnDWidget(expander)

        icon = Gtk.DragIcon.get_for_drag(drag_object)
        icon.set_child(drag_widget)

        drag_object.set_hotspot(self._drag_coords[0], self._drag_coords[1])

    def _on_drop_hover(self,
                       controller: Gtk.DropControllerMotion,
                       x: int | None = None,
                       y: int | None = None,) -> None:

        expander = controller.get_widget()

        # add exceptions do draged section and main section top

        if (x and y):

            row = expander.get_list_row()
            position = row.get_position()

            if (self._selection.is_selected(position)):
                return

            height = expander.get_height()
            zone = height // 3
            expander.add_css_class("on-drop-hover")

            if (y <= zone
                    and position > 0):  # top zone
                expander.remove_css_class("on-drop-center-hover")
                expander.remove_css_class("on-drop-bottom-hover")
                expander.add_css_class("on-drop-top-hover")
                # add to parent before this
                return

            elif (height - zone < y
                    and position > 0):  # bottom zone

                expander.remove_css_class("on-drop-center-hover")
                expander.remove_css_class("on-top-bottom-hover")
                expander.add_css_class("on-drop-bottom-hover")
                # add to parent after this
                return

            else:
                expander.remove_css_class("on-drop-top-hover")
                expander.remove_css_class("on-drop-bottom-hover")

                tree_list_row = expander.get_list_row()

                # if not (tree_list_row.is_expandable()):
                expander.add_css_class("on-drop-center-hover")

                if not (tree_list_row.get_expanded()):
                    tree_list_row.set_expanded(True)
                return

        expander.remove_css_class("on-drop-top-hover")
        expander.remove_css_class("on-drop-bottom-hover")
        expander.remove_css_class("on-drop-hover")
        expander.remove_css_class("on-drop-center-hover")

    def _on_drop(self, _drop, drag_target, x, y, drop_target):

        if (not drag_target or not drop_target
                or drag_target == drop_target):

            return False

        height = drop_target.get_height()
        zone = height // 3

        drag_row = drag_target.get_list_row()
        drop_row = drop_target.get_list_row()
        drop_section = drop_row.get_item()
        drag_section = drag_row.get_item()

        self._remove_section(drag_row, True)

        position = -1
        parent = drop_section

        if (y <= zone):  # top zone

            position = drop_section.get_relative_position()
            parent = drop_section.parent

        if (height - zone < y):  # bottom zone
            position = drop_section.get_relative_position() + 1
            parent = drop_section.parent

        self.add_section(drag_section, parent, position)

        return True

    def test(self, event, key,
             keycode, state) -> None:
        print(key, ', ', keycode)

        print("key pressed")
        # print(position, removed, added)

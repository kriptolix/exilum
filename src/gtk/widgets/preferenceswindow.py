import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Adw 


@Gtk.Template(resource_path='/io/gitlab/kriptolix'
              '/Exilium/src/gtk/ui/PreferencesWindow.ui')
class PreferencesWindow(Adw.PreferencesWindow):
    
    __gtype_name__ = 'PreferencesWindow'

    _dark_mode_switch = Gtk.Template.Child()

    def __init__(self):

        super().__init__()
# Copyright 2024 k
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw, Gtk, Gio, Gdk, GObject

from .menus import ApplicationMenu
from .panedbox import PanedBox


@Gtk.Template(resource_path='/io/gitlab/kriptolix/'
              'Exilium/src/gtk/ui/MainWindow.ui')
class MainWindow(Adw.ApplicationWindow):

    __gtype_name__ = 'MainWindow'

    _paned_box = Gtk.Template.Child()
    _sections_button = Gtk.Template.Child()
    _menu_button = Gtk.Template.Child()
    _header_bar = Gtk.Template.Child()
    _versions_button = Gtk.Template.Child()
    _notes_button = Gtk.Template.Child()
    _search_replace_button = Gtk.Template.Child()

    focus_mode = GObject.Property(type=bool, default=False)

    def __init__(self, app):

        super().__init__(application=app)

        self.application = app        

        self._search_replace_button.connect("clicked",
                                            self._paned_box._toggle_search_replace)

        self.set_size_request(360, 294)

        self._sections_button.connect("clicked",
                                      self._paned_box._toggle_panel)

        self._versions_button.connect("clicked",
                                      self._paned_box._toggle_panel)

        self._notes_button.connect("clicked",
                                   self._paned_box._toggle_panel)

        self.settings = Gio.Settings(schema_id="io.gitlab.kriptolix.Exilium")
        self.settings.bind("window-width", self, "default-width",
                           Gio.SettingsBindFlags.DEFAULT)
        self.settings.bind("window-height", self, "default-height",
                           Gio.SettingsBindFlags.DEFAULT)
        self.settings.bind("window-maximized", self, "maximized",
                           Gio.SettingsBindFlags.DEFAULT)

        css_provider = Gtk.CssProvider()
        css_provider.load_from_resource('/io/gitlab/kriptolix/'
                                        'Exilium/data/exilium.css')
        add_provider = Gtk.StyleContext.add_provider_for_display
        add_provider(Gdk.Display.get_default(),
                     css_provider,
                     Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        self._menu_button.props.popover = ApplicationMenu()

    def _on_project_opened(self):

        self._sections_button.set_visible(True)
        self._versions_button.set_visible(True)
        self._notes_button.set_visible(True)

        # self._search_replace_button.set_visible(False)

        self._sections_button.set_active(True)

        self._paned_box._right_stack.set_visible_child(
            self._paned_box._edition_view)
        self._paned_box._show_sidebar()

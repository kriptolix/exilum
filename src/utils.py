from gi.repository import Gtk, Gio

import time
import threading


def create_action(action_group, prefix, name, callback,
                  shortcuts=None, parameter=None, target=None):
    """
    Add an action to an action group.

    Parameters
    ----------
    action_group : Gio.SimpleActionGroup
        Where the actions is added
    prefix : str 
        Prefix used to actions on action group
    name : str 
        The name of the action.
    callback : function)
        The function to be called when the action is
        activated.
    shortcuts : list, optional
        An optional list of strings representing accelerators.
    parameter : GLib.VariantType, optional
        Possibles parameters for the action.
    target : any, optional 
        A standard value to action parameter.
    """

    action = Gio.SimpleAction.new(name, parameter)
    action.connect("activate", callback)
    action_group.add_action(action)
    detailed_name = f"{prefix}.{name}"

    if (target):
        detailed_name = f"{prefix}.{name}::{target}"

    if shortcuts:
        action_group.set_accels_for_action(detailed_name, shortcuts)


def create_click(widget, btn_nunber, trigger, callback, data=None):
    """
    Add an click action to a widget.

    Parameters
    ----------
    widget : Gtk.Widget
        Where the actions is added.
    btn_nunber : int
        Mouse button number that will activate de action.
    trigger : str
        Button behavior that will trigget the action.
    callback : function
        The function to be called when the action is
        activated.
    data : any, optional
        Data to pass to callback.
    """

    click = Gtk.GestureClick.new()
    click.set_button(btn_nunber)
    click.connect(trigger, callback, data)

    widget.add_controller(click)


def update_recent_projects(settings, path):
    """
    Update the recent accessed project files list from homepage.

    Parameters
    ----------
    settings : Gio.Settings
        Retrieving application settings.
    path : str
        Accessed project file path.            
    """

    recent_projects = settings.get_strv("recent-projects")

    if (path in recent_projects):
        recent_projects.remove(path)

    recent_projects.insert(0, path)

    if (len(recent_projects) >= 4):
        recent_projects.pop()

    settings.set_strv("recent-projects", recent_projects)


class SaveCountdown():
    """
    A class used to controll the time interval for autosave 

    ...

    Attributes
    ----------
    method : function
        a function to be called when time is triged    
    seconds : int
        The number seconds of a pause that trigger countdow (default is 3)
    current_seconds : int
        Hold the current time of timer
    timer : threading.Thread
        Threading Thread tha holds the timer coundown
    stop_timer : threading.Event
        Threading event tha represents timer stop    
    reset_timer : threading.Event
        Threading event tha represents timer reset

    Methods
    -------
    countdown()
        Prints the animals name and what sound it makes
    update_count()
    """

    def __init__(self, seconds, method):
        """
        Parameters
        ----------
        method : function
            a function to be called when time is triged       
        seconds : int, optional
            The number seconds of a pause that trigger countdow (default is 3)
        """

        self.method = method
        self.seconds = seconds
        self.current_seconds = seconds
        self.timer = threading.Thread(target=self._countdown)
        self.timer.daemon = True
        self.stop_timer = threading.Event()
        self.reset_timer = threading.Event()

    def _countdown(self):
        """
        Create a timer on that calls a function when get to zero
        """

        while not self.stop_timer.is_set():
            while self.current_seconds > 0 and not self.reset_timer.is_set():
                time.sleep(1)
                self.current_seconds -= 1

            if self.current_seconds == 0:
                print("tmp saved")
                self.method()

            self.reset_timer.wait()
            self.current_seconds = self.seconds
            self.reset_timer.clear()

    def update_count(self):
        """
        If timer is not active, start it. If timer is active reset it to
        maximum value
        """

        if not self.timer.is_alive():
            self.timer.start()
        else:
            self.reset_timer.set()

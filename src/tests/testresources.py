from ..gtk.widgets.sectionnode import SectionObject


def notes_test(notes_list):

    notes_title = ["Mad Max", "Tesla"]
    notes_text = ["Mad Max is a 1979 Australian dystopian action "
                  "film directed by George Miller and produced by Byron "
                  "Kennedy. Mel Gibson stars as Mad Max Rockatansky, a "
                  "police officer turned vigilante in a near-future "
                  "Australia in the midst of societal collapse. Joanne "
                  "Samuel, Hugh Keays-Byrne, Steve Bisley, Tim Burns "
                  "and Roger Ward also appear in supporting roles. James "
                  "McCausland and Miller wrote the screenplay from a "
                  "story by Miller and Kennedy",

                  "Nikola Tesla was a Serbian-American[5][6] inventor, "
                  "electrical engineer, mechanical engineer, and futurist. "
                  "He is best-known for his contributions to the design "
                  "of the modern alternating current (AC) electricity "
                  "supply system.[7]",

                  "A dune is a landform composed of wind- or water-driven "
                  "sand. It typically takes the form of a mound, ridge, or "
                  "hill.[1] An area with dunes is called a dune system or "
                  "a dune complex.[6] A large dune complex is called a dune "
                  "field,[7] while broad, flat regions covered with "
                  "wind-swept sand or dunes, with little or no vegetation"
                  ", are called ergs or sand seas."
                  ]

    notes_list._add_note(None, [notes_title[0], notes_text[0], None])
    notes_list._add_note(None, ['', notes_text[2], None])
    notes_list._add_note(None, [notes_title[1], notes_text[1], None])
    notes_list._add_note(None, ['', notes_text[2], None])


def sections_test(sections_list):

    section_titles = ["The book of flowers",
                      "Chapter 1 - Etymology",
                      "Chapter 2 - Morphology",
                      "Perianth", "Calyx", "Corolla",
                      "Chapter 3 - Reproductive"]

    sections_text = ["A flower, also known as a bloom or blossom, "
                     "is the reproductive structure found in "
                     "flowering plants (plants of the division "
                     "Angiospermae). Flowers consist of a combination "
                     "of vegetative organs – sepals that enclose and "
                     "protect the developing flower, petals that attract "
                     "pollinators, and reproductive organs that produce "
                     "gametophytes, which in flowering plants produce "
                     "gametes. The male gametophytes, which produce "
                     "sperm, are enclosed within pollen grains produced "
                     "in the anthers. The female gametophytes are "
                     "contained within the ovules produced in the carpels."
                     "\n\n A flower, also known as a bloom or blossom, "
                     "is the reproductive structure found in "
                     "flowering plants (plants of the division "
                     "Angiospermae). Flowers consist of a combination "
                     "of vegetative organs – sepals that enclose and "
                     "protect the developing flower, petals that attract "
                     "pollinators, and reproductive organs that produce "
                     "gametophytes, which in flowering plants produce "
                     "gametes. The male gametophytes, which produce "
                     "sperm, are enclosed within pollen grains produced "
                     "in the anthers. The female gametophytes are "
                     "contained within the ovules produced in the carpels."
                     "\n\n A flower, also known as a bloom or blossom, "
                     "is the reproductive structure found in "
                     "flowering plants (plants of the division "
                     "Angiospermae). Flowers consist of a combination "
                     "of vegetative organs – sepals that enclose and "
                     "protect the developing flower, petals that attract "
                     "pollinators, and reproductive organs that produce "
                     "gametophytes, which in flowering plants produce "
                     "gametes. The male gametophytes, which produce "
                     "sperm, are enclosed within pollen grains produced "
                     "in the anthers. The female gametophytes are "
                     "contained within the ovules produced in the carpels.",

                     "Flower is from the Middle English flour, which "
                     "referred to both the ground grain and the "
                     "reproductive structure in plants, before splitting "
                     "off in the 17th century. It comes originally from "
                     "the Latin name of the Italian goddess of flowers, "
                     "Flora. The early word for flower in English was "
                     "blossom,[4] though it now refers to flowers only "
                     "of fruit trees.[5]",

                     "The morphology of a flower, or its form and "
                     "structure,[6] can be considered in two parts: "
                     "the vegetative part, consisting of non-reproductive "
                     "structures such as petals; and the reproductive or "
                     "sexual parts.",

                     "The perianth (perigonium, perigon or perigone in "
                     "monocots) is the non-reproductive part of the "
                     "flower, and structure that forms an envelope "
                     "surrounding the sexual organs, consisting of the "
                     "calyx (sepals) and the corolla (petals) or tepals "
                     "when called a perigone.",

                     "The sepals, collectively called the calyx, are "
                     "modified leaves that occur on the outermost whorl "
                     "of the flower. They are leaf-like, in that they "
                     "have a broad base, stomata, stipules, and "
                     "chlorophyll.[9] Sepals are often waxy and tough"
                     ", and grow quickly to protect the flower as it "
                     "develops.[9][10] They may be deciduous, but will "
                     "more commonly grow on to assist in fruit dispersal"
                     ". If the calyx is fused together it is called "
                     "gamosepalous.[9]"
                     "\n\n The sepals, collectively called the calyx, are "
                     "modified leaves that occur on the outermost whorl "
                     "of the flower. They are leaf-like, in that they "
                     "have a broad base, stomata, stipules, and "
                     "chlorophyll.[9] Sepals are often waxy and tough"
                     ", and grow quickly to protect the flower as it "
                     "develops.[9][10] They may be deciduous, but will "
                     "more commonly grow on to assist in fruit dispersal"
                     ". If the calyx is fused together it is called "
                     "gamosepalous.[9]"
                     "\n\n The sepals, collectively called the calyx, are "
                     "modified leaves that occur on the outermost whorl "
                     "of the flower. They are leaf-like, in that they "
                     "have a broad base, stomata, stipules, and "
                     "chlorophyll.[9] Sepals are often waxy and tough"
                     ", and grow quickly to protect the flower as it "
                     "develops.[9][10] They may be deciduous, but will "
                     "more commonly grow on to assist in fruit dispersal"
                     ". If the calyx is fused together it is called "
                     "gamosepalous.[9]",

                     "The petals, together the corolla, are almost or "
                     "completely fiberless leaf-like structures that "
                     "form the innermost whorl of the perianth. "
                     "They are often delicate and thin, and are usually"
                     "coloured, shaped, or scented to encourage "
                     "pollination.[11] Although similar to leaves in "
                     "shape, they are more comparable to stamens in "
                     "that they form almost simultaneously with one "
                     "another, but their subsequent growth is delayed.",

                     "Plant reproductive morphology is the study of "
                     "the physical form and structure (the morphology) "
                     "of those parts of plants directly or indirectly "
                     "concerned with sexual reproduction."

                     ]

    main_section = SectionObject(section_titles[0])

    sections_list.add_section(main_section, None)

    cap1 = SectionObject(section_titles[1])

    sections_list.add_section(cap1, main_section)

    subcap1 = SectionObject(section_titles[3])

    sections_list.add_section(subcap1, cap1)

    cap2 = SectionObject(section_titles[2])

    sections_list.add_section(cap2, main_section)

    subcap2 = SectionObject(section_titles[4])

    sections_list.add_section(subcap2, cap2)

    subcap3 = SectionObject(section_titles[5])

    sections_list.add_section(subcap3, subcap2)

    cap3 = SectionObject(section_titles[6])

    sections_list.add_section(cap3, main_section)

    main_section.text_buffer.set_text(sections_text[0], -1)

    cap1.text_buffer.set_text(sections_text[1], -1)
    cap2.text_buffer.set_text(sections_text[2], -1)
    cap3.text_buffer.set_text(sections_text[6], -1)

    subcap1.text_buffer.set_text(sections_text[3], -1)
    subcap2.text_buffer.set_text(sections_text[4], -1)
    subcap3.text_buffer.set_text(sections_text[5], -1)


def versions_test(versions_list):

    versions_list._add_version("In progress version",
                               "Last time saved: Never",
                               "")

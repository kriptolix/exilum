import xml.etree.ElementTree as xmlet
from .gtk.widgets.sectionnode import SectionObject


def _apply_tags(section, xml_section):

    xml_tags = xml_section.find("tags")

    for xml_tag in xml_tags.findall('tag'):

        xml_name = xml_tag.find("name")
        xml_open = xml_tag.find("open")
        xml_close = xml_tag.find("close")

        start = section.text_buffer.get_iter_at_offset(
            int(xml_open.text))
        end = section.text_buffer.get_iter_at_offset(
            int(xml_close.text))
        section.text_buffer.apply_tag_by_name(xml_name.text, start, end)


def populate_sections(sections_list, xml_situation):

    def _iterate_over(xml_section, parent):

        xml_title = xml_section.find("title")
        xml_text = xml_section.find("text")

        title = xml_title.text
        text = xml_text.text

        section = SectionObject(title)
        
        sections_list.add_section(section, parent)

        if (text):

            section.text_buffer.set_text(text)            

        # _apply_tags(section, xml_section)

        parent = section

        if (xml_section.findall('section')):
            for child in xml_section.findall('section'):
                _iterate_over(child, parent)

    ##

    xml_sections = xml_situation.find("sections")
    xml_section = xml_sections.find("section")
    parent = None

    _iterate_over(xml_section, parent)


def populate_notes(notes_list, sections_text, xml_situation):

    xml_notes = xml_situation.find("notes")

    xml_notes_list = xml_notes.findall('note')

    if (xml_notes_list):

        xml_notes_list.reverse()

        for xml_note in xml_notes_list:

            xml_title = xml_note.find("title")
            xml_text = xml_note.find("text")
            xml_tag = xml_note.find("tag")
            # xml_edge = xml_note.find("edge")

            title = xml_title.text
            text = xml_text.text
            tag = xml_tag.text

            notes_list._add_note(None, [title, text, tag])

            # if (tag):

            #    TagManager._create_tag(tag, sections_text._tag_table)


def populate_versions(versions_list, xml_project):

    xml_versions = xml_project.find("versions")

    xml_versions_list = xml_versions.findall('version')
    xml_versions_list.reverse()

    for xml_version in xml_versions_list:

        xml_identifier = xml_version.find("identifier")
        xml_message = xml_version.find("message")
        xml_patch = xml_version.find("patch")

        identifier = xml_identifier.text
        message = xml_message.text
        patch = xml_patch.text

        versions_list._add_version(message, identifier, patch)

    versions_list._add_version("In progress version",
                               "Last time saved: Never",
                               "")


def deserialize_project(xml_project,
                        sections_list,
                        sections_text,
                        notes_list,
                        versions_list):

    xml_situation = xml_project.find("situation")

    populate_sections(sections_list, xml_situation)
    populate_notes(notes_list, sections_text, xml_situation)
    populate_versions(versions_list, xml_project)


def deserialize_situation(xml_situation,
                          sections_list,
                          sections_text,
                          notes_list):

    populate_sections(sections_list, xml_situation)
    populate_notes(notes_list, sections_text, xml_situation)

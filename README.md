# Exilium

![Exilum Screnns](./data/screenshots/home-dark.png?raw=true "Screnns")

Exilium is a visual session-based text editor with versioning capabilities, focused on producing long texts like books and novels. Exilium currently only works with plain text but there are plans to support formatted text and perhaps images

ℹ️ Exilium is made possible by Flatpak. Only Flathub version is supported.

## Current project status

This project already has the minimum features to be considered functional but it has not yet reached the set of features planned for a 1.0 version. Some of the features now are:

* Create, rename, move and delete individual text sections.
* Create, modify and delete notes.
* Save versioned content in a single file.
* Restore project to any earlier version saved.
* Export content as plain text.
* Zoom in/Zoom out text.
* Autosave.

### Blocking bugs

Issues to be resolved to release version 0.1.0:

* When changing the session selection, the text does not expand unless it is clicked.

### Structure

Exilium is being built using Flatpak, Python, GTK4 and libadwaita, in addition to trying to respect Gnome HIGs.

To compile the project just clone this repository and run using Gnome Builder or VScode with the Flatpak plugin.

## Roadmap

Some of the features planned for future versions are:

* Text search and replacement.
* Spell checking.
* Export content as pdf, doc, odt and other formats.
* Metrics such as writing time, number of words, etc.
* Create sessions from selected text.
* Create notes linked to portions of text.
* Support full text formatting.
* Support visualization of differences between versions.

### Contributions

I'm an amateur programmer, this is my first real project with GTK4. I'm still learning about the technologies being used in it and many of the solutions used are suboptimal. That said, contributions are welcome.
